﻿Public Class Form1
    Dim nameRecipe As String
    Dim description As String
    Dim code% = 0
    Dim md As Double
    Dim id_md%
    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'TODO: данная строка кода позволяет загрузить данные в таблицу "ProductsDBDataSet.Ing". При необходимости она может быть перемещена или удалена.
        Me.IngTableAdapter.Fill(Me.ProductsDBDataSet.Ing)
        'TODO: данная строка кода позволяет загрузить данные в таблицу "ProductsDBDataSet.ViewRecept". При необходимости она может быть перемещена или удалена.
        'Me.ViewReceptTableAdapter.Fill(Me.ProductsDBDataSet.ViewRecept)
        Me.RecipesTableAdapter.Fill(Me.ProductsDBDataSet.Recipes)
        DataGridView1.DefaultCellStyle.Format = "0.###"
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) 
        'If nameRecipe <> RecipesDataGridView.SelectedCells.Item(0).OwningRow.Cells(1).Value Then
        '    nameRecipe = RecipesDataGridView.SelectedCells.Item(0).OwningRow.Cells(1).Value
        '    Me.RecipesTableAdapter.UpdateName(nameRecipe, code)
        '    MsgBox("Изменено названия рецепта")
        'End If

        'If description <> ОписаниеTextBox.Text Then
        '    description = ОписаниеTextBox.Text
        '    Me.RecipesTableAdapter.UpdateDescription(description, code)
        '    MsgBox("Изменено описание рецепта")
        'End If
    End Sub
    Private Sub RecipesDataGridView_CellBeginEdit(sender As Object, e As DataGridViewCellCancelEventArgs) Handles RecipesDataGridView.CellBeginEdit
        nameRecipe = RecipesDataGridView.Rows(e.RowIndex).Cells(e.ColumnIndex).OwningRow.Cells(1).Value
        'description = ОписаниеTextBox.Text
    End Sub
    Private Sub RecipesDataGridView_CellEndEdit(sender As Object, e As DataGridViewCellEventArgs) Handles RecipesDataGridView.CellEndEdit
        If nameRecipe <> RecipesDataGridView.Rows(e.RowIndex).Cells(e.ColumnIndex).OwningRow.Cells(1).Value Then
            nameRecipe = RecipesDataGridView.Rows(e.RowIndex).Cells(e.ColumnIndex).OwningRow.Cells(1).Value
            Me.RecipesTableAdapter.UpdateName(nameRecipe, RecipesDataGridView.Rows(e.RowIndex).Cells(e.ColumnIndex).OwningRow.Cells(0).Value)
            MsgBox("Изменено названия рецепта")
        End If


    End Sub
    Private Sub RecipesDataGridView_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles RecipesDataGridView.CellClick
        'Dim tempCode As Long = code
        code = RecipesDataGridView.SelectedCells.Item(0).OwningRow.Cells(0).Value
        'nameRecipe = RecipesDataGridView.SelectedCells.Item(0).OwningRow.Cells(1).Value
        'description = ОписаниеTextBox.Text
        ОписаниеTextBox.Text = RecipesTableAdapter.ScalarQuery(code)

        Dim dgv As New ViewRecipeOnDGV(DataGridView1, New clRecipe(code))

        'Dim ds As ProductsDBDataSet.ViewReceptDataTable

    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        frmEditorRecipe.Show()
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        If code <> 0 Then
            frmViewAnalytic.code = code
            frmViewAnalytic.ShowDialog()
        Else
            MsgBox("Сперва выберите рецепт")
        End If
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        Dim frm As New frmEditorRecipe
        If code <> 0 Then
            frm.code = code
            frm.ShowDialog()
            Me.RecipesTableAdapter.Fill(Me.ProductsDBDataSet.Recipes)
        Else
            MsgBox("Сперва выберите рецепт")
        End If
    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        If code <> 0 Then
            Dim result% = 0
            result = MessageBox.Show("Будет удалён рецепт и всё его содержимое. Вы уверены что хотите удалить рецепт?", "Сообщение", MessageBoxButtons.YesNo)
            If result = 6 Then
                Me.RecipesTableAdapter.DeleteRecipe(code)
                Me.RecipesTableAdapter.Fill(Me.ProductsDBDataSet.Recipes)
                MsgBox("Рецепт удалён")
            End If
        Else
            MsgBox("Выберите рецепт")
        End If
    End Sub
    Private Sub ОписаниеTextBox_MouseMove(sender As Object, e As EventArgs) Handles ОписаниеTextBox.MouseMove
        If description <> ОписаниеTextBox.Text Then
            description = ОписаниеTextBox.Text
            'Me.RecipesTableAdapter.UpdateDescription(description, code)
        End If
    End Sub

    Private Sub Button1_Click_1(sender As Object, e As EventArgs) Handles Button1.Click
        'If code <> 0 Then
        '    Dim form As New SettingsOptim
        '    form.ControlRecipe = New clRecipe(code)
        '    form.Show()
        'Else
        '    MsgBox("Выберите рецепт для оптимизации")
        'End If
        If code <> 0 Then
            Dim form As New inputSettingsOptim
            form.code = Me.code
            form.Show()
        Else
            MsgBox("Выберите рецепт для оптимизации")
        End If
    End Sub

    Private Sub Form1_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        Главное_меню.Show()
    End Sub
End Class
﻿Imports ProjectUMeat.frmSimplex
Public Class BazisOption
    Private bazis#()
    Private notbazis#()

    Public x() As structBazis

    Sub New(BazisLength%)
        ReDim x(BazisLength - 1)
        For i = 0 To BazisLength - 1
            x(i).index = i
        Next
    End Sub
    Public Function GetBazis() As Integer
        Array.FindAll(x, Function(x) x.inBazis = True)
    End Function
    Public Sub Swap(indPerRow%, indPerColumn%)
        Dim swap1%
        Dim swap2%
        swap1 = Array.FindIndex(x, Function(x) x.index = indPerColumn And x.inBazis = False)
        swap2 = Array.FindIndex(x, Function(x) x.index = indPerRow And x.inBazis = True)
        If swap1 <> -1 Then
            x(swap1).index = indPerRow
            x(swap1).inBazis = True
        End If
        If swap2 <> -1 Then
            x(swap2).index = indPerColumn
            x(swap2).inBazis = False
        End If
        x = x
    End Sub
    Private Sub swapBazis(index%)
        Dim b As Boolean = x(index).inBazis
        If b Then
            frmSimplex.Log(x(index).index & " переходит в небазис")
            x(index).inBazis = False
        Else
            frmSimplex.Log(x(index).index & " переходит в базис")
            x(index).inBazis = True
        End If
    End Sub
    Public Function GetCof() As Double

    End Function
    Public Sub output()
        Dim bazis As New Stack(Of Integer), notbazis As New Stack(Of Integer)
        For Each p As structBazis In x
            If p.inBazis Then
                bazis.Push(p.index)
            Else
                notbazis.Push(p.index)
            End If
        Next
        frmSimplex.outputBazis(bazis.ToArray)
        frmSimplex.outputNotBazis(notbazis.ToArray)
    End Sub
End Class

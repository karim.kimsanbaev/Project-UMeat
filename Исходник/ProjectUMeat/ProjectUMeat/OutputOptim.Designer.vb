﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class OutputOptim
    Inherits System.Windows.Forms.Form

    'Форма переопределяет dispose для очистки списка компонентов.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Является обязательной для конструктора форм Windows Forms
    Private components As System.ComponentModel.IContainer

    'Примечание: следующая процедура является обязательной для конструктора форм Windows Forms
    'Для ее изменения используйте конструктор форм Windows Form.  
    'Не изменяйте ее в редакторе исходного кода.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.dgvRecipe1 = New System.Windows.Forms.DataGridView()
        Me.dgvRecipe2 = New System.Windows.Forms.DataGridView()
        Me.ZedGraphControl1 = New ZedGraph.ZedGraphControl()
        Me.dgvConfines = New System.Windows.Forms.DataGridView()
        Me.ZedGraphControl2 = New ZedGraph.ZedGraphControl()
        CType(Me.dgvRecipe1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvRecipe2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvConfines, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'dgvRecipe1
        '
        Me.dgvRecipe1.AllowUserToResizeRows = False
        Me.dgvRecipe1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvRecipe1.BackgroundColor = System.Drawing.Color.Gray
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.DarkGray
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.Gold
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvRecipe1.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvRecipe1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle2.BackColor = System.Drawing.Color.White
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Monotype Corsiva", 14.25!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvRecipe1.DefaultCellStyle = DataGridViewCellStyle2
        Me.dgvRecipe1.Location = New System.Drawing.Point(12, 12)
        Me.dgvRecipe1.Name = "dgvRecipe1"
        Me.dgvRecipe1.RowHeadersVisible = False
        Me.dgvRecipe1.Size = New System.Drawing.Size(520, 246)
        Me.dgvRecipe1.TabIndex = 7
        '
        'dgvRecipe2
        '
        Me.dgvRecipe2.AllowUserToResizeRows = False
        Me.dgvRecipe2.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvRecipe2.BackgroundColor = System.Drawing.Color.Gray
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle3.BackColor = System.Drawing.Color.DarkGray
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.Color.Gold
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvRecipe2.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.dgvRecipe2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle4.BackColor = System.Drawing.Color.White
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Monotype Corsiva", 14.25!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvRecipe2.DefaultCellStyle = DataGridViewCellStyle4
        Me.dgvRecipe2.Location = New System.Drawing.Point(610, 12)
        Me.dgvRecipe2.Name = "dgvRecipe2"
        Me.dgvRecipe2.RowHeadersVisible = False
        Me.dgvRecipe2.Size = New System.Drawing.Size(483, 246)
        Me.dgvRecipe2.TabIndex = 8
        '
        'ZedGraphControl1
        '
        Me.ZedGraphControl1.Location = New System.Drawing.Point(12, 276)
        Me.ZedGraphControl1.Name = "ZedGraphControl1"
        Me.ZedGraphControl1.ScrollGrace = 0R
        Me.ZedGraphControl1.ScrollMaxX = 0R
        Me.ZedGraphControl1.ScrollMaxY = 0R
        Me.ZedGraphControl1.ScrollMaxY2 = 0R
        Me.ZedGraphControl1.ScrollMinX = 0R
        Me.ZedGraphControl1.ScrollMinY = 0R
        Me.ZedGraphControl1.ScrollMinY2 = 0R
        Me.ZedGraphControl1.Size = New System.Drawing.Size(520, 275)
        Me.ZedGraphControl1.TabIndex = 9
        '
        'dgvConfines
        '
        Me.dgvConfines.AllowUserToResizeRows = False
        Me.dgvConfines.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvConfines.BackgroundColor = System.Drawing.Color.Gray
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle5.BackColor = System.Drawing.Color.DarkGray
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        DataGridViewCellStyle5.ForeColor = System.Drawing.Color.Gold
        DataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvConfines.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle5
        Me.dgvConfines.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle6.BackColor = System.Drawing.Color.White
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Monotype Corsiva", 14.25!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        DataGridViewCellStyle6.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvConfines.DefaultCellStyle = DataGridViewCellStyle6
        Me.dgvConfines.Location = New System.Drawing.Point(1099, 101)
        Me.dgvConfines.Name = "dgvConfines"
        Me.dgvConfines.RowHeadersVisible = False
        Me.dgvConfines.Size = New System.Drawing.Size(283, 157)
        Me.dgvConfines.TabIndex = 10
        '
        'ZedGraphControl2
        '
        Me.ZedGraphControl2.Location = New System.Drawing.Point(610, 276)
        Me.ZedGraphControl2.Name = "ZedGraphControl2"
        Me.ZedGraphControl2.ScrollGrace = 0R
        Me.ZedGraphControl2.ScrollMaxX = 0R
        Me.ZedGraphControl2.ScrollMaxY = 0R
        Me.ZedGraphControl2.ScrollMaxY2 = 0R
        Me.ZedGraphControl2.ScrollMinX = 0R
        Me.ZedGraphControl2.ScrollMinY = 0R
        Me.ZedGraphControl2.ScrollMinY2 = 0R
        Me.ZedGraphControl2.Size = New System.Drawing.Size(483, 275)
        Me.ZedGraphControl2.TabIndex = 11
        '
        'OutputOptim
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.DimGray
        Me.ClientSize = New System.Drawing.Size(1407, 580)
        Me.Controls.Add(Me.ZedGraphControl2)
        Me.Controls.Add(Me.dgvConfines)
        Me.Controls.Add(Me.ZedGraphControl1)
        Me.Controls.Add(Me.dgvRecipe2)
        Me.Controls.Add(Me.dgvRecipe1)
        Me.Name = "OutputOptim"
        Me.Text = "Вывод результатов"
        CType(Me.dgvRecipe1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvRecipe2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvConfines, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents dgvRecipe1 As DataGridView
    Friend WithEvents dgvRecipe2 As DataGridView
    Friend WithEvents ZedGraphControl1 As ZedGraph.ZedGraphControl
    Friend WithEvents dgvConfines As DataGridView
    Friend WithEvents ZedGraphControl2 As ZedGraph.ZedGraphControl
End Class

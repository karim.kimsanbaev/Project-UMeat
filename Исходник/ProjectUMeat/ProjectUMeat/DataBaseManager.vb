﻿Public Module DataBaseManager
    Dim IngredientsTableAdapter As New ProductsDBDataSetTableAdapters.IngredientsTableAdapter
    Dim RecipesTableAdapter As New ProductsDBDataSetTableAdapters.RecipesTableAdapter
    Dim IngTableAdapter As New ProductsDBDataSetTableAdapters.IngTableAdapter
    'БЛОК РАБОТЫ С РЕЦЕПТАМИ
    Public Function FillCompositionRecipeByCode(code As Long) As clRecipe.structCompositionRecipe()
        If CheckCode(code) Then
            Return Nothing
        End If

        Dim DataTableRecipe As DataTable = GetDataTableRecipeByCode(code)

        Dim composition() As clRecipe.structCompositionRecipe
        composition = GetEmptyCompositionArray(DataTableRecipe)

        For i% = 0 To composition.Length - 1
            Dim codeIngredient As Long = DataTableRecipe.Rows(i).Item("Код ингредиента")
            composition(i).Ingredient = New clIngredient(codeIngredient)
            composition(i).Md = DataTableRecipe.Rows(i).Item("Массовая доля") / 1000
        Next
        Return composition
    End Function
    Private Function GetDataTableRecipeByCode(code As Long) As DataTable
        Dim DataTableRecipe As ProductsDBDataSet.IngDataTable
        DataTableRecipe = IngTableAdapter.FillDataRecipeByCode(code)
        Return DataTableRecipe
    End Function
    Private Function GetEmptyCompositionArray(DataTableRecipe As DataTable) As clRecipe.structCompositionRecipe()
        Dim sizeComposition% = DataTableRecipe.Rows.Count
        Dim composition(sizeComposition - 1) As clRecipe.structCompositionRecipe
        Return composition
    End Function
    Public Function GetNameRecipeByCode(Optional code As Long = 0) As String
        If CheckCode(code) Then
            Return Nothing
        End If
        Return RecipesTableAdapter.GetNameRecipe(code)
    End Function
    'БЛОК РАБОТЫ С ИНГРЕДИЕНТАМИ
    Public Function GetЭЦпоумолчанию(Optional code% = 0) As Double
        Dim dt As ProductsDBDataSet.IngredientsRow
        dt = IngredientsTableAdapter.GetЭЦandССВ(code).Item(0)
        Return dt.Item("ЭЦ")
    End Function
    Public Function GetССВ(Optional code% = 0) As Double
        Dim dt As ProductsDBDataSet.IngredientsRow
        dt = IngredientsTableAdapter.GetЭЦandССВ(code).Item(0)
        Return dt.Item("Содержание_сухих_веществ")
    End Function
    Public Sub WriteIngredientToDB(ingredient As clIngredient)
        Try
            IngredientsTableAdapter.InsertIngredient(
            ingredient.name, ingredient.components.вода, ingredient.components.белок, ingredient.components.жиры, ingredient.components.углеводы,
            ingredient.components.клечатка, ingredient.components.зола, ingredient.components.изолейц, ingredient.components.лейц, ingredient.components.валин, ingredient.components.мет_цист,
            ingredient.components.фен_тир, ingredient.components.триптофан, ingredient.components.лизин, ingredient.components.треон, ingredient.price
        )
        Catch ex As Exception
            MsgBox("Ошибка в добавлении ингредиентов")
        End Try
    End Sub
    Public Sub WriteRecipeToDB(recipe As clRecipe)
        Dim code% = 0
        Try

            RecipesTableAdapter.InsertNewRecipe(recipe.name, "")

            code = RecipesTableAdapter.ScalarMaxCode()

            For Each x As clRecipe.structCompositionRecipe In recipe.CompositionRecipe
                IngTableAdapter.InsertIng(code, x.Ingredient.code, x.Md)

            Next
        Catch ex As Exception
            MsgBox("Ошибка в добавлении рецепта")
        End Try
        MsgBox("Рецепт сохранён")
    End Sub
    Public Function GetNameIngredientByCode(Optional code As Long = 0) As String
        If CheckCode(code) Then
            Return Nothing
        End If
        Dim name As String
        name = IngredientsTableAdapter.GetNameByCode(code)
        Return name
    End Function
    Public Function GetCodeIngredientByName(Optional name As String = "") As Long
        If CheckName(name) Then
            Return 0
        End If
        Return IngredientsTableAdapter.GetCode(name)
    End Function
    Public Function GetPriceIngredientByCode(code As Long) As Double
        If CheckCode(code) Then
            Return Nothing
        End If

        Dim row As ProductsDBDataSet.IngredientsRow
        row = IngredientsTableAdapter.GetIngByCode(code).Rows.Item(0)

        Return row.Цена
    End Function
    Public Function GetComponentsByCode(Optional code As Long = 0) As clIngredient.structComponents
        If CheckCode(code) Then
            Return Nothing
        End If
        'Изымаем строку с данными ингредиента
        Dim row As DataRow
        row = GetRowIngredient(code)
        'Заполняем структуру класса из строки с данными ингредиента
        Dim IngredientComponents As clIngredient.structComponents
        IngredientComponents = FillComponentsByRow(row)
        Return IngredientComponents
    End Function
    Private Function GetRowIngredient(code As Long)
        Dim row As ProductsDBDataSet.IngredientsRow
        row = IngredientsTableAdapter.GetIngByCode(code).Rows.Item(0)
        Return row
    End Function
    Private Function FillComponentsByRow(row As ProductsDBDataSet.IngredientsRow) As clIngredient.structComponents 'Заполнить экземпляр объекта из строки полученной по запросу из БД
        Dim IngredientComponents As New clIngredient.structComponents
        Try
            IngredientComponents.вода = row.Item("Вода")
            IngredientComponents.белок = row.Item("Белок")
            IngredientComponents.жиры = row.Item("Жиры")
            IngredientComponents.углеводы = row.Item("Углеводы")

            'IngredientComponents. = row.Item("Клечатка")
            'IngredientComponents.белок = row.Item("Зола")
            IngredientComponents.изолейц = row.Item("Изолейц")
            IngredientComponents.лейц = row.Item("Лейц")
            IngredientComponents.валин = row.Item("Валин")
            IngredientComponents.мет_цист = row.Item("Мет+Цист")
            IngredientComponents.фен_тир = row.Item("Фен+Тир")
            IngredientComponents.триптофан = row.Item("Триптофан")
            IngredientComponents.лизин = row.Item("Лизин")
            IngredientComponents.треон = row.Item("Треон")
        Catch ex As Exception
            MsgBox("Неверное название столбца при заполнении ингредиента")
        End Try

        Return IngredientComponents
    End Function
    'БЛОК ПРОВЕРОК
    Private Function CheckCode(code As Long) As Boolean
        If code = 0 Then
            MessageBox.Show("Код ингредиента не задан!", "Ошибка модуля управления с БД", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return True
        Else
            Return False
        End If
    End Function
    Private Function CheckName(name As String) As Boolean
        If name = "" Then
            MessageBox.Show("Имя ингредиента не задано!", "Ошибка модуля управления с БД", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return True
        Else
            Return False
        End If
    End Function
End Module

﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmCreateRecipe
    Inherits System.Windows.Forms.Form

    'Форма переопределяет dispose для очистки списка компонентов.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Является обязательной для конструктора форм Windows Forms
    Private components As System.ComponentModel.IContainer

    'Примечание: следующая процедура является обязательной для конструктора форм Windows Forms
    'Для ее изменения используйте конструктор форм Windows Form.  
    'Не изменяйте ее в редакторе исходного кода.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim НазваниеLabel As System.Windows.Forms.Label
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.НазваниеListBox = New System.Windows.Forms.ListBox()
        Me.IngredientsBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ProductsDBDataSet = New ProjectUMeat.ProductsDBDataSet()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.IngredientsTableAdapter = New ProjectUMeat.ProductsDBDataSetTableAdapters.IngredientsTableAdapter()
        Me.TableAdapterManager = New ProjectUMeat.ProductsDBDataSetTableAdapters.TableAdapterManager()
        Me.RecipesTableAdapter1 = New ProjectUMeat.ProductsDBDataSetTableAdapters.RecipesTableAdapter()
        Me.IngTableAdapter1 = New ProjectUMeat.ProductsDBDataSetTableAdapters.IngTableAdapter()
        Me.IngBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Button3 = New System.Windows.Forms.Button()
        Me.dgvRecipe = New System.Windows.Forms.DataGridView()
        НазваниеLabel = New System.Windows.Forms.Label()
        CType(Me.IngredientsBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ProductsDBDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.IngBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvRecipe, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'НазваниеLabel
        '
        НазваниеLabel.AutoSize = True
        НазваниеLabel.Location = New System.Drawing.Point(9, 9)
        НазваниеLabel.Name = "НазваниеLabel"
        НазваниеLabel.Size = New System.Drawing.Size(78, 13)
        НазваниеLabel.TabIndex = 1
        НазваниеLabel.Text = "Ингредиенты:"
        '
        'НазваниеListBox
        '
        Me.НазваниеListBox.DataSource = Me.IngredientsBindingSource
        Me.НазваниеListBox.DisplayMember = "Название"
        Me.НазваниеListBox.FormattingEnabled = True
        Me.НазваниеListBox.Location = New System.Drawing.Point(12, 32)
        Me.НазваниеListBox.Name = "НазваниеListBox"
        Me.НазваниеListBox.Size = New System.Drawing.Size(279, 446)
        Me.НазваниеListBox.TabIndex = 2
        '
        'IngredientsBindingSource
        '
        Me.IngredientsBindingSource.DataMember = "Ingredients"
        Me.IngredientsBindingSource.DataSource = Me.ProductsDBDataSet
        '
        'ProductsDBDataSet
        '
        Me.ProductsDBDataSet.DataSetName = "ProductsDBDataSet"
        Me.ProductsDBDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Button1
        '
        Me.Button1.Image = Global.ProjectUMeat.My.Resources.Resources.unnamed1
        Me.Button1.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Button1.Location = New System.Drawing.Point(307, 360)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(118, 56)
        Me.Button1.TabIndex = 7
        Me.Button1.Text = "Добавить ингредиент"
        Me.Button1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Image = Global.ProjectUMeat.My.Resources.Resources.save
        Me.Button2.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Button2.Location = New System.Drawing.Point(431, 360)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(118, 56)
        Me.Button2.TabIndex = 8
        Me.Button2.Text = "Сохранить рецептуру"
        Me.Button2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.SystemColors.Control
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.Label1.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label1.Location = New System.Drawing.Point(670, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(57, 20)
        Me.Label1.TabIndex = 9
        Me.Label1.Text = "Label1"
        '
        'IngredientsTableAdapter
        '
        Me.IngredientsTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.IngredientsTableAdapter = Me.IngredientsTableAdapter
        Me.TableAdapterManager.IngTableAdapter = Nothing
        Me.TableAdapterManager.RecipesTableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = ProjectUMeat.ProductsDBDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'RecipesTableAdapter1
        '
        Me.RecipesTableAdapter1.ClearBeforeFill = True
        '
        'IngTableAdapter1
        '
        Me.IngTableAdapter1.ClearBeforeFill = True
        '
        'IngBindingSource
        '
        Me.IngBindingSource.DataMember = "Ing"
        Me.IngBindingSource.DataSource = Me.ProductsDBDataSet
        '
        'Button3
        '
        Me.Button3.Image = Global.ProjectUMeat.My.Resources.Resources.appinventor_ai_artjkl23_calc
        Me.Button3.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Button3.Location = New System.Drawing.Point(307, 422)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(118, 56)
        Me.Button3.TabIndex = 10
        Me.Button3.Text = "Аналитика"
        Me.Button3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button3.UseVisualStyleBackColor = True
        '
        'dgvRecipe
        '
        Me.dgvRecipe.AllowUserToAddRows = False
        Me.dgvRecipe.AllowUserToDeleteRows = False
        Me.dgvRecipe.AllowUserToResizeColumns = False
        Me.dgvRecipe.AllowUserToResizeRows = False
        Me.dgvRecipe.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvRecipe.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvRecipe.BackgroundColor = System.Drawing.Color.Gray
        Me.dgvRecipe.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Monotype Corsiva", 14.25!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvRecipe.DefaultCellStyle = DataGridViewCellStyle1
        Me.dgvRecipe.Location = New System.Drawing.Point(307, 32)
        Me.dgvRecipe.MultiSelect = False
        Me.dgvRecipe.Name = "dgvRecipe"
        Me.dgvRecipe.ReadOnly = True
        Me.dgvRecipe.RowHeadersVisible = False
        Me.dgvRecipe.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvRecipe.Size = New System.Drawing.Size(695, 259)
        Me.dgvRecipe.TabIndex = 11
        '
        'frmCreateRecipe
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.DimGray
        Me.ClientSize = New System.Drawing.Size(1284, 637)
        Me.Controls.Add(Me.dgvRecipe)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(НазваниеLabel)
        Me.Controls.Add(Me.НазваниеListBox)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmCreateRecipe"
        Me.Text = "Редактор рецептов"
        CType(Me.IngredientsBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ProductsDBDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.IngBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvRecipe, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents ProductsDBDataSet As ProductsDBDataSet
    Friend WithEvents IngredientsBindingSource As BindingSource
    Friend WithEvents IngredientsTableAdapter As ProductsDBDataSetTableAdapters.IngredientsTableAdapter
    Friend WithEvents TableAdapterManager As ProductsDBDataSetTableAdapters.TableAdapterManager
    Friend WithEvents НазваниеListBox As ListBox
    Friend WithEvents Button1 As Button
    Friend WithEvents Button2 As Button
    Friend WithEvents Label1 As Label
    Friend WithEvents RecipesTableAdapter1 As ProductsDBDataSetTableAdapters.RecipesTableAdapter
    Friend WithEvents IngTableAdapter1 As ProductsDBDataSetTableAdapters.IngTableAdapter
    Friend WithEvents IngBindingSource As BindingSource
    Friend WithEvents Button3 As Button
    Friend WithEvents dgvRecipe As DataGridView
End Class

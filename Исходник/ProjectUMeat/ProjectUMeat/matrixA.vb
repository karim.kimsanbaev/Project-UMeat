﻿Imports ProjectUMeat.frmSimplex
Public Class matrixA
    Private A()() As Double
    Dim width%
    Dim height%
    Sub New(width%)
        frmSimplex.Log("Строю таблицу коэффицентов")
        frmSimplex.Log("Количество столбцов = " & width)

        Me.width = width
        A = New Double(width - 1)() {}
    End Sub
    Private Sub AddRow(arr#())
        If arr.Length <> width Then
            Throw New Exception("Размер массива не совпадает с шириной таблицы коэффицентов")
        End If

        Array.Resize(A, height + 1)
        A(height) = New Double(arr.Length) {}
        arr.CopyTo(A(height), 0)
        height += 1
    End Sub
    Public Sub buildMatrix(confines() As structConfines)
        'Array.Resize(confines, confines.Length - 1)
        Array.ForEach(confines, action:=Sub(x) AddRow(x.valueCom))
    End Sub
    Public Function GetColumn(index%) As Double()
        Dim arrColumn(height - 1) As Double
        Dim j As ParamArrayAttribute
        For i = 0 To height - 1
            arrColumn(i) = A(i)(index)
        Next
        Return arrColumn
    End Function
    Public Function GetRow(indRow%) As Double()
        Return A(indRow)
    End Function
    Public Function GetNonMinusMemColumn(index%) As Double()
        Dim arrColumn#() = GetColumn(index)
        Dim arrResult#() = arrColumn.Select(Function(x)
                                                If x > 0 Then : Return x
                                                Else : Return 0 : End If
                                            End Function).ToArray
        Return arrResult
    End Function
    Public Function GetNonMinusMemRow(index%) As Double()
        Dim arrColumn#() = GetRow(index)
        Dim arrResult#() = arrColumn.Select(Function(x)
                                                If x < 0 Then : Return x
                                                Else : Return 0 : End If
                                            End Function).ToArray
        Return arrResult
    End Function
    Public Sub output()
        frmSimplex.outputA(A, width, height)
    End Sub
    Private Function GetWidth() As Integer

    End Function
    Private Function GetHeight() As Integer

    End Function

    Private Sub SetValue2(x#, row%, column%)
        A(row)(column) = x
    End Sub
    Public Function GetValue(row%, column%) As Double
        If row > height Or column > width Then
            MsgBox("Ошибка взятия значения и за матрицы коэффифентов")
            Return 0
        End If
        Return A(row)(column)
    End Function
    Public Sub RecountFreeMem(xrs As cell)

        For i = 0 To height - 1
            If i = xrs.perRow Then
                Continue For
            End If
            For j = 0 To width - 1
                If j = xrs.perColumn Then
                    Continue For
                End If
                Dim xij# = A(i)(j)
                Dim xis# = A(i)(xrs.perColumn)
                Dim xrj# = A(xrs.perRow)(j)
                A(i)(j) = evaluateFreeMem(xrs.value, xij, xis, xrj)
            Next
        Next
    End Sub
    Public Sub RecountPerRow(xrs As cell)
        For i% = 0 To width - 1
            Dim x# = A(xrs.perRow)(i)
            A(xrs.perRow)(i) = x / xrs.value
            frmSimplex.Log("Расчёт " & x & "Итог: " & x * xrs.value)
        Next
        'Array.ForEach(A(xrs.perRow), Function(x)
        '                                 frmSimplex.Log("Расчёт " & x & "Итог: " & x / xrs.value)
        '                                 Return x / xrs.value
        '                             End Function)
    End Sub
    Public Sub RecountPerColumn(xrs As cell)
        For i = 0 To height - 1
            A(i)(xrs.perColumn) = A(i)(xrs.perColumn) / -xrs.value
        Next
    End Sub
    Public Sub RecountXrs(xrs As cell)
        If xrs.value <> 0 Then
            A(xrs.perRow)(xrs.perColumn) = 1 / xrs.value
        Else
            MsgBox("Разрешающий элементе равен 0")
        End If
    End Sub
    Private Function evaluateFreeMem(xrs#, xij#, xis#, xrj#) As Double
        Return (xrs * xij - xis * xrj) / xrs
    End Function
End Class

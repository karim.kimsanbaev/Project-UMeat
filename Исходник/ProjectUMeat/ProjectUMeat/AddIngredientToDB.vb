﻿Public Class AddIngredientToDB
    Private Sub FillIngredient(ingredient As clIngredient)
        FillMainComponents(ingredient)
        FillAminocisloty(ingredient)
    End Sub
    Private Sub FillMainComponents(ingredient As clIngredient)
        Try
            ingredient.components.вода = isEmpty(txtWater)
            ingredient.components.белок = isEmpty(txtBelok)
            ingredient.components.жиры = isEmpty(txtJir)
            ingredient.components.углеводы = isEmpty(txtUglevody)
        Catch ex As Exception
            MsgBox("Ошибка в основных компонентах")
            ingredient.components.вода = 0
            ingredient.components.белок = 0
            ingredient.components.жиры = 0
            ingredient.components.углеводы = 0
        End Try
    End Sub
    Private Sub FillAminocisloty(ingredient As clIngredient)
        Try
            ingredient.components.изолейц = isEmpty(txtIzo)
            ingredient.components.лейц = isEmpty(txtLei)
            ingredient.components.валин = isEmpty(txtValin)
            ingredient.components.мет_цист = isEmpty(txtMet_Cist)
            ingredient.components.фен_тир = isEmpty(txtFen_Tir)
            ingredient.components.триптофан = isEmpty(txtTriptofan)
            ingredient.components.треон = isEmpty(txtTreon)
            ingredient.components.лизин = isEmpty(txtLizin)
        Catch ex As Exception
            MsgBox("Ошибка в Аминокислотах")
            ingredient.components.изолейц = 0
            ingredient.components.лейц = 0
            ingredient.components.валин = 0
            ingredient.components.мет_цист = 0
            ingredient.components.фен_тир = 0
            ingredient.components.триптофан = 0
            ingredient.components.треон = 0
            ingredient.components.лизин = 0
        End Try
    End Sub
    Private Sub btnOptim_Click(sender As Object, e As EventArgs) Handles btnOptim.Click
        If txtName.Text = "" Then
            MsgBox("Название ингредиента не может быть пустым!")
            Return
        End If
        Dim name As String = txtName.Text
        Dim ingredient As New clIngredient(name)
        FillIngredient(ingredient)

        DataBaseManager.WriteIngredientToDB(ingredient)
        MsgBox("Ингредиент " & ingredient.name & " сохранён")
        Me.Close()
    End Sub
    Private Function isEmpty(textbox As TextBox) As Double

        Dim str As String
        If textbox.Text = "" Then
            Return 0
        Else
            str = textbox.Text
            If str.Contains(".") Then
                str = str.Replace(".", ",")
                textbox.Text = str
                Return isEmpty(textbox)
            End If
        End If
        If textbox.Text = "," Then
            Return 0
        End If
        Return CDbl(str)
    End Function

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        Me.Close()
    End Sub
End Class
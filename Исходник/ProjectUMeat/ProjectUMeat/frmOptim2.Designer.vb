﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmOptim2
    Inherits System.Windows.Forms.Form

    'Форма переопределяет dispose для очистки списка компонентов.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Является обязательной для конструктора форм Windows Forms
    Private components As System.ComponentModel.IContainer

    'Примечание: следующая процедура является обязательной для конструктора форм Windows Forms
    'Для ее изменения используйте конструктор форм Windows Form.  
    'Не изменяйте ее в редакторе исходного кода.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim НазваниеLabel As System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label()
        Me.ProductsDBDataSet = New ProjectUMeat.ProductsDBDataSet()
        Me.ViewReceptBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ViewReceptTableAdapter = New ProjectUMeat.ProductsDBDataSetTableAdapters.ViewReceptTableAdapter()
        Me.TableAdapterManager = New ProjectUMeat.ProductsDBDataSetTableAdapters.TableAdapterManager()
        Me.IngredientsTableAdapter1 = New ProjectUMeat.ProductsDBDataSetTableAdapters.IngredientsTableAdapter()
        Me.txtPerZam = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.IngredientsBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.IngredientsBindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.IngBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.IngTableAdapter = New ProjectUMeat.ProductsDBDataSetTableAdapters.IngTableAdapter()
        Me.НазваниеComboBox = New System.Windows.Forms.ComboBox()
        НазваниеLabel = New System.Windows.Forms.Label()
        CType(Me.ProductsDBDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ViewReceptBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.IngredientsBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.IngredientsBindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.IngBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(205, 57)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(39, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Label1"
        '
        'ProductsDBDataSet
        '
        Me.ProductsDBDataSet.DataSetName = "ProductsDBDataSet"
        Me.ProductsDBDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'ViewReceptBindingSource
        '
        Me.ViewReceptBindingSource.DataMember = "ViewRecept"
        Me.ViewReceptBindingSource.DataSource = Me.ProductsDBDataSet
        '
        'ViewReceptTableAdapter
        '
        Me.ViewReceptTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.IngredientsTableAdapter = Me.IngredientsTableAdapter1
        Me.TableAdapterManager.IngTableAdapter = Nothing
        Me.TableAdapterManager.RecipesTableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = ProjectUMeat.ProductsDBDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'IngredientsTableAdapter1
        '
        Me.IngredientsTableAdapter1.ClearBeforeFill = True
        '
        'txtPerZam
        '
        Me.txtPerZam.Location = New System.Drawing.Point(190, 175)
        Me.txtPerZam.Name = "txtPerZam"
        Me.txtPerZam.Size = New System.Drawing.Size(100, 20)
        Me.txtPerZam.TabIndex = 4
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(72, 178)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(112, 13)
        Me.Label2.TabIndex = 5
        Me.Label2.Text = "Процент замещения"
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(190, 234)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(161, 78)
        Me.Button1.TabIndex = 6
        Me.Button1.Text = "Добавить экспериментальный ингредиент"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'IngredientsBindingSource
        '
        Me.IngredientsBindingSource.DataMember = "Ingredients"
        Me.IngredientsBindingSource.DataSource = Me.ProductsDBDataSet
        '
        'IngredientsBindingSource1
        '
        Me.IngredientsBindingSource1.DataMember = "Ingredients"
        Me.IngredientsBindingSource1.DataSource = Me.ProductsDBDataSet
        '
        'IngBindingSource
        '
        Me.IngBindingSource.DataMember = "Ing"
        Me.IngBindingSource.DataSource = Me.ProductsDBDataSet
        '
        'IngTableAdapter
        '
        Me.IngTableAdapter.ClearBeforeFill = True
        '
        'НазваниеLabel
        '
        НазваниеLabel.AutoSize = True
        НазваниеLabel.Location = New System.Drawing.Point(124, 116)
        НазваниеLabel.Name = "НазваниеLabel"
        НазваниеLabel.Size = New System.Drawing.Size(60, 13)
        НазваниеLabel.TabIndex = 6
        НазваниеLabel.Text = "Название:"
        '
        'НазваниеComboBox
        '
        Me.НазваниеComboBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ViewReceptBindingSource, "Название", True))
        Me.НазваниеComboBox.DataSource = Me.ViewReceptBindingSource
        Me.НазваниеComboBox.DisplayMember = "Название"
        Me.НазваниеComboBox.FormattingEnabled = True
        Me.НазваниеComboBox.Location = New System.Drawing.Point(190, 113)
        Me.НазваниеComboBox.Name = "НазваниеComboBox"
        Me.НазваниеComboBox.Size = New System.Drawing.Size(121, 21)
        Me.НазваниеComboBox.TabIndex = 7
        '
        'frmOptim2
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(583, 426)
        Me.Controls.Add(НазваниеLabel)
        Me.Controls.Add(Me.НазваниеComboBox)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.txtPerZam)
        Me.Controls.Add(Me.Label1)
        Me.Name = "frmOptim2"
        Me.Text = "frmOptim2"
        CType(Me.ProductsDBDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ViewReceptBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.IngredientsBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.IngredientsBindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.IngBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label1 As Label
    Friend WithEvents ProductsDBDataSet As ProductsDBDataSet
    Friend WithEvents ViewReceptBindingSource As BindingSource
    Friend WithEvents ViewReceptTableAdapter As ProductsDBDataSetTableAdapters.ViewReceptTableAdapter
    Friend WithEvents TableAdapterManager As ProductsDBDataSetTableAdapters.TableAdapterManager
    Friend WithEvents IngredientsTableAdapter1 As ProductsDBDataSetTableAdapters.IngredientsTableAdapter
    Friend WithEvents txtPerZam As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents Button1 As Button
    Friend WithEvents IngredientsBindingSource As BindingSource
    Friend WithEvents IngredientsBindingSource1 As BindingSource
    Friend WithEvents IngBindingSource As BindingSource
    Friend WithEvents IngTableAdapter As ProductsDBDataSetTableAdapters.IngTableAdapter
    Friend WithEvents НазваниеComboBox As ComboBox
End Class

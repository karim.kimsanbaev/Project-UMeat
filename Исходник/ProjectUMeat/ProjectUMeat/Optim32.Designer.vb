﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Optim32
    Inherits System.Windows.Forms.Form

    'Форма переопределяет dispose для очистки списка компонентов.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Является обязательной для конструктора форм Windows Forms
    Private components As System.ComponentModel.IContainer

    'Примечание: следующая процедура является обязательной для конструктора форм Windows Forms
    'Для ее изменения используйте конструктор форм Windows Form.  
    'Не изменяйте ее в редакторе исходного кода.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.НазваниеDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ВодаDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.БелокDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ЖирыDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.УглеводыDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.КлечаткаDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ЗолаDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ИзолейцDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ЛейцDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ВалинDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.МетЦистDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ФенТирDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ТриптофанDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ЛизинDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ТреонDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ЦенаDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IngredientsBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ProductsDBDataSet = New ProjectUMeat.ProductsDBDataSet()
        Me.add_ingr = New System.Windows.Forms.Button()
        Me.КодDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TableAdapterManager = New ProjectUMeat.ProductsDBDataSetTableAdapters.TableAdapterManager()
        Me.IngredientsTableAdapter = New ProjectUMeat.ProductsDBDataSetTableAdapters.IngredientsTableAdapter()
        Me.DataGridView4 = New System.Windows.Forms.DataGridView()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.Dgv1 = New System.Windows.Forms.DataGridView()
        Me.Название = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.От = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.до = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.cmbcomp = New System.Windows.Forms.ComboBox()
        Me.cmbway = New System.Windows.Forms.ComboBox()
        Me.optcomp = New System.Windows.Forms.GroupBox()
        Me.energ_deg = New System.Windows.Forms.CheckBox()
        Me.prot = New System.Windows.Forms.CheckBox()
        Me.fats = New System.Windows.Forms.CheckBox()
        Me.Carb = New System.Windows.Forms.CheckBox()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.Ингредиент = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Массовая_доля = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridView3 = New System.Windows.Forms.DataGridView()
        Me.optimiz = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        CType(Me.IngredientsBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ProductsDBDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataGridView4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox3.SuspendLayout()
        CType(Me.Dgv1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        Me.optcomp.SuspendLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataGridView3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox4.SuspendLayout()
        Me.SuspendLayout()
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.Red
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.Button5.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Button5.Location = New System.Drawing.Point(215, 0)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(30, 23)
        Me.Button5.TabIndex = 19
        Me.Button5.Text = "X"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'НазваниеDataGridViewTextBoxColumn
        '
        Me.НазваниеDataGridViewTextBoxColumn.DataPropertyName = "Название"
        Me.НазваниеDataGridViewTextBoxColumn.HeaderText = "Название"
        Me.НазваниеDataGridViewTextBoxColumn.Name = "НазваниеDataGridViewTextBoxColumn"
        '
        'ВодаDataGridViewTextBoxColumn
        '
        Me.ВодаDataGridViewTextBoxColumn.DataPropertyName = "Вода"
        Me.ВодаDataGridViewTextBoxColumn.HeaderText = "Вода"
        Me.ВодаDataGridViewTextBoxColumn.Name = "ВодаDataGridViewTextBoxColumn"
        '
        'БелокDataGridViewTextBoxColumn
        '
        Me.БелокDataGridViewTextBoxColumn.DataPropertyName = "Белок"
        Me.БелокDataGridViewTextBoxColumn.HeaderText = "Белок"
        Me.БелокDataGridViewTextBoxColumn.Name = "БелокDataGridViewTextBoxColumn"
        '
        'ЖирыDataGridViewTextBoxColumn
        '
        Me.ЖирыDataGridViewTextBoxColumn.DataPropertyName = "Жиры"
        Me.ЖирыDataGridViewTextBoxColumn.HeaderText = "Жиры"
        Me.ЖирыDataGridViewTextBoxColumn.Name = "ЖирыDataGridViewTextBoxColumn"
        '
        'УглеводыDataGridViewTextBoxColumn
        '
        Me.УглеводыDataGridViewTextBoxColumn.DataPropertyName = "Углеводы"
        Me.УглеводыDataGridViewTextBoxColumn.HeaderText = "Углеводы"
        Me.УглеводыDataGridViewTextBoxColumn.Name = "УглеводыDataGridViewTextBoxColumn"
        '
        'КлечаткаDataGridViewTextBoxColumn
        '
        Me.КлечаткаDataGridViewTextBoxColumn.DataPropertyName = "Клечатка"
        Me.КлечаткаDataGridViewTextBoxColumn.HeaderText = "Клечатка"
        Me.КлечаткаDataGridViewTextBoxColumn.Name = "КлечаткаDataGridViewTextBoxColumn"
        '
        'ЗолаDataGridViewTextBoxColumn
        '
        Me.ЗолаDataGridViewTextBoxColumn.DataPropertyName = "Зола"
        Me.ЗолаDataGridViewTextBoxColumn.HeaderText = "Зола"
        Me.ЗолаDataGridViewTextBoxColumn.Name = "ЗолаDataGridViewTextBoxColumn"
        '
        'ИзолейцDataGridViewTextBoxColumn
        '
        Me.ИзолейцDataGridViewTextBoxColumn.DataPropertyName = "Изолейц"
        Me.ИзолейцDataGridViewTextBoxColumn.HeaderText = "Изолейц"
        Me.ИзолейцDataGridViewTextBoxColumn.Name = "ИзолейцDataGridViewTextBoxColumn"
        '
        'ЛейцDataGridViewTextBoxColumn
        '
        Me.ЛейцDataGridViewTextBoxColumn.DataPropertyName = "Лейц"
        Me.ЛейцDataGridViewTextBoxColumn.HeaderText = "Лейц"
        Me.ЛейцDataGridViewTextBoxColumn.Name = "ЛейцDataGridViewTextBoxColumn"
        '
        'ВалинDataGridViewTextBoxColumn
        '
        Me.ВалинDataGridViewTextBoxColumn.DataPropertyName = "Валин"
        Me.ВалинDataGridViewTextBoxColumn.HeaderText = "Валин"
        Me.ВалинDataGridViewTextBoxColumn.Name = "ВалинDataGridViewTextBoxColumn"
        '
        'МетЦистDataGridViewTextBoxColumn
        '
        Me.МетЦистDataGridViewTextBoxColumn.DataPropertyName = "Мет+Цист"
        Me.МетЦистDataGridViewTextBoxColumn.HeaderText = "Мет+Цист"
        Me.МетЦистDataGridViewTextBoxColumn.Name = "МетЦистDataGridViewTextBoxColumn"
        '
        'ФенТирDataGridViewTextBoxColumn
        '
        Me.ФенТирDataGridViewTextBoxColumn.DataPropertyName = "Фен+Тир"
        Me.ФенТирDataGridViewTextBoxColumn.HeaderText = "Фен+Тир"
        Me.ФенТирDataGridViewTextBoxColumn.Name = "ФенТирDataGridViewTextBoxColumn"
        '
        'ТриптофанDataGridViewTextBoxColumn
        '
        Me.ТриптофанDataGridViewTextBoxColumn.DataPropertyName = "Триптофан"
        Me.ТриптофанDataGridViewTextBoxColumn.HeaderText = "Триптофан"
        Me.ТриптофанDataGridViewTextBoxColumn.Name = "ТриптофанDataGridViewTextBoxColumn"
        '
        'ЛизинDataGridViewTextBoxColumn
        '
        Me.ЛизинDataGridViewTextBoxColumn.DataPropertyName = "Лизин"
        Me.ЛизинDataGridViewTextBoxColumn.HeaderText = "Лизин"
        Me.ЛизинDataGridViewTextBoxColumn.Name = "ЛизинDataGridViewTextBoxColumn"
        '
        'ТреонDataGridViewTextBoxColumn
        '
        Me.ТреонDataGridViewTextBoxColumn.DataPropertyName = "Треон"
        Me.ТреонDataGridViewTextBoxColumn.HeaderText = "Треон"
        Me.ТреонDataGridViewTextBoxColumn.Name = "ТреонDataGridViewTextBoxColumn"
        '
        'ЦенаDataGridViewTextBoxColumn
        '
        Me.ЦенаDataGridViewTextBoxColumn.DataPropertyName = "Цена"
        Me.ЦенаDataGridViewTextBoxColumn.HeaderText = "Цена"
        Me.ЦенаDataGridViewTextBoxColumn.Name = "ЦенаDataGridViewTextBoxColumn"
        '
        'IngredientsBindingSource
        '
        Me.IngredientsBindingSource.DataMember = "Ingredients"
        Me.IngredientsBindingSource.DataSource = Me.ProductsDBDataSet
        '
        'ProductsDBDataSet
        '
        Me.ProductsDBDataSet.DataSetName = "ProductsDBDataSet"
        Me.ProductsDBDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'add_ingr
        '
        Me.add_ingr.Location = New System.Drawing.Point(145, 538)
        Me.add_ingr.Name = "add_ingr"
        Me.add_ingr.Size = New System.Drawing.Size(144, 43)
        Me.add_ingr.TabIndex = 25
        Me.add_ingr.Text = "Добавить ингредиент для замещения"
        Me.add_ingr.UseVisualStyleBackColor = True
        '
        'КодDataGridViewTextBoxColumn
        '
        Me.КодDataGridViewTextBoxColumn.DataPropertyName = "Код"
        Me.КодDataGridViewTextBoxColumn.HeaderText = "Код"
        Me.КодDataGridViewTextBoxColumn.Name = "КодDataGridViewTextBoxColumn"
        Me.КодDataGridViewTextBoxColumn.ReadOnly = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.IngredientsTableAdapter = Me.IngredientsTableAdapter
        Me.TableAdapterManager.IngTableAdapter = Nothing
        Me.TableAdapterManager.RecipesTableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = ProjectUMeat.ProductsDBDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'IngredientsTableAdapter
        '
        Me.IngredientsTableAdapter.ClearBeforeFill = True
        '
        'DataGridView4
        '
        Me.DataGridView4.AutoGenerateColumns = False
        Me.DataGridView4.BackgroundColor = System.Drawing.SystemColors.Control
        Me.DataGridView4.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView4.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.КодDataGridViewTextBoxColumn, Me.НазваниеDataGridViewTextBoxColumn, Me.ВодаDataGridViewTextBoxColumn, Me.БелокDataGridViewTextBoxColumn, Me.ЖирыDataGridViewTextBoxColumn, Me.УглеводыDataGridViewTextBoxColumn, Me.КлечаткаDataGridViewTextBoxColumn, Me.ЗолаDataGridViewTextBoxColumn, Me.ИзолейцDataGridViewTextBoxColumn, Me.ЛейцDataGridViewTextBoxColumn, Me.ВалинDataGridViewTextBoxColumn, Me.МетЦистDataGridViewTextBoxColumn, Me.ФенТирDataGridViewTextBoxColumn, Me.ТриптофанDataGridViewTextBoxColumn, Me.ЛизинDataGridViewTextBoxColumn, Me.ТреонDataGridViewTextBoxColumn, Me.ЦенаDataGridViewTextBoxColumn})
        Me.DataGridView4.DataSource = Me.IngredientsBindingSource
        Me.DataGridView4.Location = New System.Drawing.Point(6, 10)
        Me.DataGridView4.Name = "DataGridView4"
        Me.DataGridView4.Size = New System.Drawing.Size(155, 417)
        Me.DataGridView4.TabIndex = 18
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.Dgv1)
        Me.GroupBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.GroupBox3.ForeColor = System.Drawing.Color.Gold
        Me.GroupBox3.Location = New System.Drawing.Point(974, 271)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(359, 247)
        Me.GroupBox3.TabIndex = 21
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Ограничения по массововй доле"
        '
        'Dgv1
        '
        Me.Dgv1.AllowUserToAddRows = False
        Me.Dgv1.AllowUserToDeleteRows = False
        DataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black
        Me.Dgv1.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle2
        Me.Dgv1.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Dgv1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.Dgv1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Название, Me.От, Me.до})
        Me.Dgv1.Location = New System.Drawing.Point(8, 34)
        Me.Dgv1.Name = "Dgv1"
        Me.Dgv1.Size = New System.Drawing.Size(344, 207)
        Me.Dgv1.TabIndex = 0
        '
        'Название
        '
        Me.Название.HeaderText = "Название"
        Me.Название.Name = "Название"
        '
        'От
        '
        Me.От.HeaderText = "От"
        Me.От.Name = "От"
        '
        'до
        '
        Me.до.HeaderText = "до"
        Me.до.Name = "до"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label1.Location = New System.Drawing.Point(10, 18)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(105, 31)
        Me.Label1.TabIndex = 20
        Me.Label1.Text = "Рецепт"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.cmbcomp)
        Me.GroupBox2.Controls.Add(Me.cmbway)
        Me.GroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.GroupBox2.ForeColor = System.Drawing.Color.Gold
        Me.GroupBox2.Location = New System.Drawing.Point(974, 143)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(354, 122)
        Me.GroupBox2.TabIndex = 19
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Целевая функция"
        '
        'cmbcomp
        '
        Me.cmbcomp.FormattingEnabled = True
        Me.cmbcomp.Items.AddRange(New Object() {"Белки", "Жиры", "Углеводы", "ЭЦ"})
        Me.cmbcomp.Location = New System.Drawing.Point(6, 76)
        Me.cmbcomp.Name = "cmbcomp"
        Me.cmbcomp.Size = New System.Drawing.Size(342, 24)
        Me.cmbcomp.TabIndex = 1
        '
        'cmbway
        '
        Me.cmbway.FormattingEnabled = True
        Me.cmbway.Items.AddRange(New Object() {"Минимизировать", "Максимизировать"})
        Me.cmbway.Location = New System.Drawing.Point(6, 32)
        Me.cmbway.Name = "cmbway"
        Me.cmbway.Size = New System.Drawing.Size(342, 24)
        Me.cmbway.TabIndex = 0
        '
        'optcomp
        '
        Me.optcomp.Controls.Add(Me.energ_deg)
        Me.optcomp.Controls.Add(Me.prot)
        Me.optcomp.Controls.Add(Me.fats)
        Me.optcomp.Controls.Add(Me.Carb)
        Me.optcomp.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.optcomp.ForeColor = System.Drawing.Color.Gold
        Me.optcomp.Location = New System.Drawing.Point(974, 21)
        Me.optcomp.Name = "optcomp"
        Me.optcomp.Size = New System.Drawing.Size(348, 116)
        Me.optcomp.TabIndex = 18
        Me.optcomp.TabStop = False
        Me.optcomp.Text = "Оптимизировать"
        '
        'energ_deg
        '
        Me.energ_deg.AutoSize = True
        Me.energ_deg.ForeColor = System.Drawing.SystemColors.ControlText
        Me.energ_deg.Location = New System.Drawing.Point(10, 86)
        Me.energ_deg.Name = "energ_deg"
        Me.energ_deg.Size = New System.Drawing.Size(222, 20)
        Me.energ_deg.TabIndex = 3
        Me.energ_deg.Text = "Энергетическая ценность"
        Me.energ_deg.UseVisualStyleBackColor = True
        '
        'prot
        '
        Me.prot.AutoSize = True
        Me.prot.ForeColor = System.Drawing.SystemColors.ControlText
        Me.prot.Location = New System.Drawing.Point(8, 14)
        Me.prot.Name = "prot"
        Me.prot.Size = New System.Drawing.Size(72, 20)
        Me.prot.TabIndex = 0
        Me.prot.Text = "Белки"
        Me.prot.UseVisualStyleBackColor = True
        '
        'fats
        '
        Me.fats.AutoSize = True
        Me.fats.ForeColor = System.Drawing.SystemColors.ControlText
        Me.fats.Location = New System.Drawing.Point(8, 40)
        Me.fats.Name = "fats"
        Me.fats.Size = New System.Drawing.Size(69, 20)
        Me.fats.TabIndex = 1
        Me.fats.Text = "Жиры"
        Me.fats.UseVisualStyleBackColor = True
        '
        'Carb
        '
        Me.Carb.AutoSize = True
        Me.Carb.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Carb.Location = New System.Drawing.Point(8, 63)
        Me.Carb.Name = "Carb"
        Me.Carb.Size = New System.Drawing.Size(99, 20)
        Me.Carb.TabIndex = 2
        Me.Carb.Text = "Углеводы"
        Me.Carb.UseVisualStyleBackColor = True
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        Me.DataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.DataGridView1.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Ингредиент, Me.Массовая_доля})
        Me.DataGridView1.GridColor = System.Drawing.Color.Gray
        Me.DataGridView1.Location = New System.Drawing.Point(10, 61)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.Size = New System.Drawing.Size(544, 442)
        Me.DataGridView1.TabIndex = 17
        '
        'Ингредиент
        '
        Me.Ингредиент.HeaderText = "Ингредиент"
        Me.Ингредиент.Name = "Ингредиент"
        '
        'Массовая_доля
        '
        Me.Массовая_доля.HeaderText = "Массовая доля"
        Me.Массовая_доля.Name = "Массовая_доля"
        '
        'DataGridView3
        '
        Me.DataGridView3.AllowUserToAddRows = False
        Me.DataGridView3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView3.Location = New System.Drawing.Point(63, 158)
        Me.DataGridView3.Name = "DataGridView3"
        Me.DataGridView3.Size = New System.Drawing.Size(155, 150)
        Me.DataGridView3.TabIndex = 23
        '
        'optimiz
        '
        Me.optimiz.Location = New System.Drawing.Point(16, 538)
        Me.optimiz.Name = "optimiz"
        Me.optimiz.Size = New System.Drawing.Size(112, 43)
        Me.optimiz.TabIndex = 22
        Me.optimiz.Text = "Оптимизировать"
        Me.optimiz.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(161, 43)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(75, 23)
        Me.Button2.TabIndex = 14
        Me.Button2.Text = "Добавить"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.Button5)
        Me.GroupBox4.Controls.Add(Me.DataGridView4)
        Me.GroupBox4.Controls.Add(Me.Button2)
        Me.GroupBox4.Controls.Add(Me.DataGridView3)
        Me.GroupBox4.Location = New System.Drawing.Point(560, 61)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(245, 442)
        Me.GroupBox4.TabIndex = 24
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Ингредиент для замены"
        '
        'Optim32
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1354, 687)
        Me.Controls.Add(Me.add_ingr)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.optcomp)
        Me.Controls.Add(Me.DataGridView1)
        Me.Controls.Add(Me.optimiz)
        Me.Controls.Add(Me.GroupBox4)
        Me.Name = "Optim32"
        Me.Text = "Optim32"
        CType(Me.IngredientsBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ProductsDBDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataGridView4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox3.ResumeLayout(False)
        CType(Me.Dgv1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.optcomp.ResumeLayout(False)
        Me.optcomp.PerformLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataGridView3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox4.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Button5 As Button
    Friend WithEvents НазваниеDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents ВодаDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents БелокDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents ЖирыDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents УглеводыDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents КлечаткаDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents ЗолаDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents ИзолейцDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents ЛейцDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents ВалинDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents МетЦистDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents ФенТирDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents ТриптофанDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents ЛизинDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents ТреонDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents ЦенаDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents IngredientsBindingSource As BindingSource
    Friend WithEvents ProductsDBDataSet As ProductsDBDataSet
    Friend WithEvents add_ingr As Button
    Friend WithEvents КодDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents TableAdapterManager As ProductsDBDataSetTableAdapters.TableAdapterManager
    Friend WithEvents IngredientsTableAdapter As ProductsDBDataSetTableAdapters.IngredientsTableAdapter
    Friend WithEvents DataGridView4 As DataGridView
    Friend WithEvents GroupBox3 As GroupBox
    Friend WithEvents Dgv1 As DataGridView
    Friend WithEvents Название As DataGridViewTextBoxColumn
    Friend WithEvents От As DataGridViewTextBoxColumn
    Friend WithEvents до As DataGridViewTextBoxColumn
    Friend WithEvents Label1 As Label
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents cmbcomp As ComboBox
    Friend WithEvents cmbway As ComboBox
    Friend WithEvents optcomp As GroupBox
    Friend WithEvents energ_deg As CheckBox
    Friend WithEvents prot As CheckBox
    Friend WithEvents fats As CheckBox
    Friend WithEvents Carb As CheckBox
    Friend WithEvents DataGridView1 As DataGridView
    Friend WithEvents Ингредиент As DataGridViewTextBoxColumn
    Friend WithEvents Массовая_доля As DataGridViewTextBoxColumn
    Friend WithEvents DataGridView3 As DataGridView
    Friend WithEvents optimiz As Button
    Friend WithEvents Button2 As Button
    Friend WithEvents GroupBox4 As GroupBox
End Class

﻿Public Class frmOptim2
    Dim codeNewIng As Long
    Dim codeRecipe As Long
    Public Sub SetIngAndRecipe(codeNewIng As Long, codeRecipe As Long)
        Me.codeNewIng = codeNewIng
        Me.codeRecipe = codeRecipe

        Label1.Text = IngredientsTableAdapter1.GetIngByCode(codeNewIng).Item(0).Item(1) 'Здесь изымается название ингредиента из целой таблицы возвращаемой функции (пересечение: 0 строка и 1 столбец)
    End Sub
    Private Sub frmOptim2_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'TODO: данная строка кода позволяет загрузить данные в таблицу "ProductsDBDataSet.Ing". При необходимости она может быть перемещена или удалена.
        Me.IngTableAdapter.Fill(Me.ProductsDBDataSet.Ing)
        'TODO: данная строка кода позволяет загрузить данные в таблицу "ProductsDBDataSet.ViewRecept". При необходимости она может быть перемещена или удалена.
        Me.ViewReceptTableAdapter.Fill(Me.ProductsDBDataSet.ViewRecept)
    End Sub
    Public recipe As New clRecipe
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        If txtPerZam.Text = "" Or Not IsNumeric(txtPerZam.Text) Then
            MsgBox("Корректно укажите процент замещения")
            Return
        End If
        If НазваниеComboBox.SelectedIndex = -1 Then
            MsgBox("Укажите замещаемый ингредиент")
            Return
        End If

        Dim IngredientsTA As New ProductsDBDataSetTableAdapters.IngredientsTableAdapter

        Dim codeZamIng As Long = 0
        Dim row As DataRow
        row = НазваниеComboBox.SelectedItem.Row
        codeZamIng = IngredientsTA.GetCode(row.Item(2))

        'Чтобы сформировать новый ингредиент, нужно:
        'Добавлять ингредиенты
        'Перебираем весь рецептурный состав
        Dim IngTA As New ProductsDBDataSetTableAdapters.IngTableAdapter

        Dim sostav As New ProductsDBDataSet.IngDataTable
        Try
            sostav = IngTA.FillDataRecipeByCode(codeRecipe)
        Catch ex As Exception
        End Try
        'MsgBox(sostav.Rows(0).Item(1))

        For i = 0 To sostav.Rows.Count - 1
            'Если ингредиент замещаемый:
            If sostav.Rows(i).Item(1) = codeZamIng Then
                'MsgBox("Замещаемый ингредиент")
                'Берём его массовую долю, высчитываем по проценту замещения масссовую долю экспериментального ингредиента
                Dim md# = sostav.Rows(i).Item(2)
                'MsgBox("Его массовая доля")
                'MsgBox(md)
                Dim mdEx# = 0
                mdEx = md / 100 * CDbl(txtPerZam.Text)
                'Вычитаем массовую долю экспериментального от замещаемого ингредиента
                md = md - mdEx
                'MsgBox("Массовая доля экспериментального преобразования")
                'MsgBox(md)
                'Вставляем замещаемый ингредиент и его массовую долю
                recipe.InsertIng(sostav.Rows(i).Item(1), md)
                'MsgBox("Параметры вставленного замещаемого ингредиента")
                'MsgBox(IngredientsTA.GetNameByCode(sostav.Rows(i).Item(1))) : MsgBox(md)
                'Выставляем экспериментальный ингредиент и его массовую долю
                recipe.InsertExIng(codeNewIng, mdEx)
                'MsgBox("Параметры вставленного нового ингредиента")
                'MsgBox(IngredientsTA.GetNameByCode(codeNewIng)) : MsgBox(mdEx)
            Else
                'MsgBox("Обычный ингредиент") : MsgBox(sostav.Rows(i).Item(1))
                Dim md# = sostav.Rows(i).Item(2)
                'MsgBox("Его массовая доля")
                'MsgBox(md)
                recipe.InsertIng(sostav.Rows(i).Item(1), md)
                'MsgBox("Параметры вставленного обычного ингредиента")
                'MsgBox(IngredientsTA.GetNameByCode(sostav.Rows(i).Item(1))) : MsgBox(md)
            End If
        Next
        frmViewAnalytic.Show()
        'Выводим всё в DataTable
        'Подсоединяем DataTable в DataGridView
        'Сверяем результаты
    End Sub

    Private Sub FillByToolStripButton_Click(sender As Object, e As EventArgs)
        Try
            Me.IngTableAdapter.FillBy(Me.ProductsDBDataSet.Ing)
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub
End Class
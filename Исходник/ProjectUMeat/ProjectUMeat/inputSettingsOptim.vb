﻿Imports ProjectUMeat.frmSimplex
Public Class inputSettingsOptim
    Public code% = 30050
    'Dim code% = 20049
    Dim recipe As clRecipe

    Private Sub autoView4()
        cmbF.SelectedIndex = 1
        RadioButton1.Checked = True
        cmbБелок.SelectedIndex = 1
        txtБелок.Text = 21
        cmb2.SelectedIndex = 1
        txtЖир.Text = 30
        cmb3.SelectedIndex = 1
        txtUgl.Text = 500
    End Sub
    Private Sub autoView()
        cmbF.SelectedIndex = 0
        RadioButton2.Checked = True
        cmbБелок.SelectedIndex = 0
        txtБелок.Text = 3
        cmb2.SelectedIndex = 1
        txtЖир.Text = 1
        cmb3.SelectedIndex = 0
        txtUgl.Text = 6
    End Sub
    Private Sub autoView2()
        cmbF.SelectedIndex = 1
        RadioButton1.Checked = True

        cmbБелок.SelectedIndex = 0
        txtБелок.Text = 19

        cmb2.SelectedIndex = 0
        txtЖир.Text = 13

        cmb3.SelectedIndex = 0
        txtUgl.Text = 15

        cmbССВ.SelectedIndex = 0
        txtССВ.Text = 18
    End Sub
    Private Sub autoView3()
        cmbF.SelectedIndex = 1
        RadioButton2.Checked = True

        cmbБелок.SelectedIndex = 0
        txtБелок.Text = 3

        cmb2.SelectedIndex = 1
        txtЖир.Text = 1

        cmb3.SelectedIndex = 0
        txtUgl.Text = 3
    End Sub

    Private Sub inputSettingsOptim_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        recipe = New clRecipe(code)
        'recipe.ЭЦandССВ()

        recipe.OutputDGV(dgvRecipe)
        recipe.OutputDGV(DataGridView1)

        viewDGVsettingMD()

        autoView4()
    End Sub
    Private Sub viewDGVsettingMD()
        dgvSettingMD.Columns.Add("Название", "Ингредиент")
        dgvSettingMD.Columns.Add("ОТ", "ОТ")
        dgvSettingMD.Columns.Add("ДО", "ДО")

        For i% = 0 To recipe.CompositionRecipe.Length - 1
            Dim row As New DataGridViewRow
            row.CreateCells(dgvSettingMD)
            row.Cells(0).Value = recipe.CompositionRecipe(i).Ingredient.name
            row.Cells(1).Value = 0
            row.Cells(2).Value = Double.PositiveInfinity

            dgvSettingMD.Rows.Add(row)
        Next
        dgvSettingMD.Columns(0).DefaultCellStyle.Font = New System.Drawing.Font("Arial", 10, FontStyle.Bold)
        dgvSettingMD.Columns(0).Width = 150
    End Sub

    Private Sub btnOptim_Click(sender As Object, e As EventArgs) Handles btnOptim.Click
        Dim lenComRec% = recipe.CompositionRecipe.Length - 1
        Dim confines As New Stack(Of structConfines)

        Dim temp As structConfines
        Try
            temp.name = "Белки"
            temp.equality = cmbБелок.Text
            temp.valueCom = getbelok()
            temp.value = CDbl(txtБелок.Text)
            confines.Push(temp)

            temp.name = "Жиры"
            temp.equality = cmb2.Text
            temp.valueCom = getjir()
            temp.value = CDbl(txtЖир.Text)
            confines.Push(temp)

            temp.name = "Углеводы"
            temp.equality = cmb3.Text
            temp.valueCom = getugl()
            temp.value = CDbl(txtUgl.Text)
            confines.Push(temp)

            'temp.name = "Содержание сухих веществ"
            'temp.equality = cmbССВ.Text
            'temp.valueCom = getССВ()
            'temp.value = CDbl(txtССВ.Text)
            'confines.Push(temp)
        Catch ex As Exception
            MsgBox("Введите корректные значения")
        End Try

        Dim obj#() = objFunction()

        'Stop

        Dim arrMDot#(lenComRec)
        Dim arrMDdo#(lenComRec)
        For i% = 0 To lenComRec - 1
            Dim arr#(lenComRec)
            arr.SetValue(1, i)

            If dgvSettingMD.Rows(i).Cells(1).Value <> 0 Then
                Dim tempOt As structConfines
                tempOt.name = recipe.CompositionRecipe(i).Ingredient.name
                tempOt.value = dgvSettingMD.Rows(i).Cells(1).Value
                tempOt.equality = ">="
                tempOt.valueCom = arr
                confines.Push(tempOt)
            End If

            If Not Double.IsInfinity(dgvSettingMD.Rows(i).Cells(2).Value) Then
                Dim tempDo As structConfines
                tempDo.name = recipe.CompositionRecipe(i).Ingredient.name
                tempDo.value = dgvSettingMD.Rows(i).Cells(2).Value
                tempDo.equality = "<="
                tempDo.valueCom = arr
                confines.Push(tempDo)
            End If
        Next
        confines = confines
        frmSimplex.start(objFunction(), confines.ToArray, recipe)
    End Sub
    Function getbelok() As Double()
        Dim arr#(recipe.CompositionRecipe.Length - 1)
        For i = 0 To arr.Length - 1
            arr(i) = recipe.CompositionRecipe(i).Ingredient.components.белок
        Next
        Return arr
    End Function
    Function getjir() As Double()
        Dim arr#(recipe.CompositionRecipe.Length - 1)
        For i = 0 To arr.Length - 1
            arr(i) = recipe.CompositionRecipe(i).Ingredient.components.жиры
        Next
        Return arr
    End Function
    Function getugl() As Double()
        Dim arr#(recipe.CompositionRecipe.Length - 1)
        For i = 0 To arr.Length - 1
            arr(i) = recipe.CompositionRecipe(i).Ingredient.components.углеводы
        Next
        Return arr
    End Function
    Function getССВ() As Double()
        Dim arr#(recipe.CompositionRecipe.Length - 1)
        For i = 0 To arr.Length - 1
            arr(i) = recipe.CompositionRecipe(i).Ingredient.Содержание_сухих_веществ
        Next
        Return arr
    End Function
    Private Function objFunction() As Double()
        Dim obj#(recipe.CompositionRecipe.Length - 1)
        If RadioButton1.Checked Then
            Debug.WriteLine("Минимизация")
            For i = 0 To recipe.CompositionRecipe.Length - 1
                obj(i) = recipe.CompositionRecipe(i).Ingredient.price * -1
            Next
        Else
            Debug.WriteLine("Максимизация")
            For i = 0 To recipe.CompositionRecipe.Length - 1
                obj(i) = recipe.CompositionRecipe(i).Ingredient.price
            Next
        End If
        Return obj
    End Function

    Private Sub Button1_Click(sender As Object, e As EventArgs)

    End Sub
End Class
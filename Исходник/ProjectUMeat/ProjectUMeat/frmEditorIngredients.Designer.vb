﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmEditorIngredients
    Inherits System.Windows.Forms.Form

    'Форма переопределяет dispose для очистки списка компонентов.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Является обязательной для конструктора форм Windows Forms
    Private components As System.ComponentModel.IContainer

    'Примечание: следующая процедура является обязательной для конструктора форм Windows Forms
    'Для ее изменения используйте конструктор форм Windows Form.  
    'Не изменяйте ее в редакторе исходного кода.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim КодLabel As System.Windows.Forms.Label
        Dim ЛейцLabel As System.Windows.Forms.Label
        Dim ВалинLabel As System.Windows.Forms.Label
        Dim ИзолейцLabel As System.Windows.Forms.Label
        Dim Мет_ЦистLabel As System.Windows.Forms.Label
        Dim ЗолаLabel As System.Windows.Forms.Label
        Dim Фен_ТирLabel As System.Windows.Forms.Label
        Dim КлечаткаLabel As System.Windows.Forms.Label
        Dim ТриптофанLabel As System.Windows.Forms.Label
        Dim ВодаLabel As System.Windows.Forms.Label
        Dim ЛизинLabel As System.Windows.Forms.Label
        Dim ТреонLabel As System.Windows.Forms.Label
        Dim Содержание_сухих_веществLabel As System.Windows.Forms.Label
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEditorIngredients))
        Me.IngredientsDataGridView = New System.Windows.Forms.DataGridView()
        Me.Название = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Цена = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ЭЦ = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Белок = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Жиры = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Углеводы = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.КодDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.НазваниеDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ВодаDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.БелокDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ЖирыDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.УглеводыDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.КлечаткаDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ЗолаDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ИзолейцDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ЛейцDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ВалинDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.МетЦистDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ФенТирDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ТриптофанDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ЛизинDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ТреонDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ЦенаDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ЭЦDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.СодержаниесухихвеществDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IngredientsBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ProductsDBDataSet = New ProjectUMeat.ProductsDBDataSet()
        Me.btnOptim = New System.Windows.Forms.Button()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.IngredientsTableAdapter = New ProjectUMeat.ProductsDBDataSetTableAdapters.IngredientsTableAdapter()
        Me.TableAdapterManager = New ProjectUMeat.ProductsDBDataSetTableAdapters.TableAdapterManager()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.КодLabel1 = New System.Windows.Forms.Label()
        Me.ТреонTextBox = New System.Windows.Forms.TextBox()
        Me.Содержание_сухих_веществTextBox = New System.Windows.Forms.TextBox()
        Me.ЛизинTextBox = New System.Windows.Forms.TextBox()
        Me.ТриптофанTextBox = New System.Windows.Forms.TextBox()
        Me.ВодаTextBox = New System.Windows.Forms.TextBox()
        Me.Фен_ТирTextBox = New System.Windows.Forms.TextBox()
        Me.КлечаткаTextBox = New System.Windows.Forms.TextBox()
        Me.Мет_ЦистTextBox = New System.Windows.Forms.TextBox()
        Me.ЗолаTextBox = New System.Windows.Forms.TextBox()
        Me.ВалинTextBox = New System.Windows.Forms.TextBox()
        Me.ИзолейцTextBox = New System.Windows.Forms.TextBox()
        Me.ЛейцTextBox = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        КодLabel = New System.Windows.Forms.Label()
        ЛейцLabel = New System.Windows.Forms.Label()
        ВалинLabel = New System.Windows.Forms.Label()
        ИзолейцLabel = New System.Windows.Forms.Label()
        Мет_ЦистLabel = New System.Windows.Forms.Label()
        ЗолаLabel = New System.Windows.Forms.Label()
        Фен_ТирLabel = New System.Windows.Forms.Label()
        КлечаткаLabel = New System.Windows.Forms.Label()
        ТриптофанLabel = New System.Windows.Forms.Label()
        ВодаLabel = New System.Windows.Forms.Label()
        ЛизинLabel = New System.Windows.Forms.Label()
        ТреонLabel = New System.Windows.Forms.Label()
        Содержание_сухих_веществLabel = New System.Windows.Forms.Label()
        CType(Me.IngredientsDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.IngredientsBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ProductsDBDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox4.SuspendLayout()
        Me.SuspendLayout()
        '
        'КодLabel
        '
        КодLabel.AutoSize = True
        КодLabel.ForeColor = System.Drawing.Color.White
        КодLabel.Location = New System.Drawing.Point(6, 23)
        КодLabel.Name = "КодLabel"
        КодLabel.Size = New System.Drawing.Size(40, 18)
        КодLabel.TabIndex = 47
        КодLabel.Text = "Код:"
        '
        'ЛейцLabel
        '
        ЛейцLabel.AutoSize = True
        ЛейцLabel.ForeColor = System.Drawing.Color.White
        ЛейцLabel.Location = New System.Drawing.Point(20, 267)
        ЛейцLabel.Name = "ЛейцLabel"
        ЛейцLabel.Size = New System.Drawing.Size(47, 18)
        ЛейцLabel.TabIndex = 33
        ЛейцLabel.Text = "Лейц:"
        '
        'ВалинLabel
        '
        ВалинLabel.AutoSize = True
        ВалинLabel.ForeColor = System.Drawing.Color.White
        ВалинLabel.Location = New System.Drawing.Point(20, 293)
        ВалинLabel.Name = "ВалинLabel"
        ВалинLabel.Size = New System.Drawing.Size(55, 18)
        ВалинLabel.TabIndex = 35
        ВалинLabel.Text = "Валин:"
        '
        'ИзолейцLabel
        '
        ИзолейцLabel.AutoSize = True
        ИзолейцLabel.ForeColor = System.Drawing.Color.White
        ИзолейцLabel.Location = New System.Drawing.Point(20, 245)
        ИзолейцLabel.Name = "ИзолейцLabel"
        ИзолейцLabel.Size = New System.Drawing.Size(73, 18)
        ИзолейцLabel.TabIndex = 31
        ИзолейцLabel.Text = "Изолейц:"
        '
        'Мет_ЦистLabel
        '
        Мет_ЦистLabel.AutoSize = True
        Мет_ЦистLabel.ForeColor = System.Drawing.Color.White
        Мет_ЦистLabel.Location = New System.Drawing.Point(20, 319)
        Мет_ЦистLabel.Name = "Мет_ЦистLabel"
        Мет_ЦистLabel.Size = New System.Drawing.Size(83, 18)
        Мет_ЦистLabel.TabIndex = 37
        Мет_ЦистLabel.Text = "Мет+Цист:"
        '
        'ЗолаLabel
        '
        ЗолаLabel.AutoSize = True
        ЗолаLabel.ForeColor = System.Drawing.Color.White
        ЗолаLabel.Location = New System.Drawing.Point(20, 180)
        ЗолаLabel.Name = "ЗолаLabel"
        ЗолаLabel.Size = New System.Drawing.Size(48, 18)
        ЗолаLabel.TabIndex = 29
        ЗолаLabel.Text = "Зола:"
        '
        'Фен_ТирLabel
        '
        Фен_ТирLabel.AutoSize = True
        Фен_ТирLabel.ForeColor = System.Drawing.Color.White
        Фен_ТирLabel.Location = New System.Drawing.Point(20, 345)
        Фен_ТирLabel.Name = "Фен_ТирLabel"
        Фен_ТирLabel.Size = New System.Drawing.Size(75, 18)
        Фен_ТирLabel.TabIndex = 39
        Фен_ТирLabel.Text = "Фен+Тир:"
        '
        'КлечаткаLabel
        '
        КлечаткаLabel.AutoSize = True
        КлечаткаLabel.ForeColor = System.Drawing.Color.White
        КлечаткаLabel.Location = New System.Drawing.Point(20, 150)
        КлечаткаLabel.Name = "КлечаткаLabel"
        КлечаткаLabel.Size = New System.Drawing.Size(78, 18)
        КлечаткаLabel.TabIndex = 27
        КлечаткаLabel.Text = "Клечатка:"
        '
        'ТриптофанLabel
        '
        ТриптофанLabel.AutoSize = True
        ТриптофанLabel.ForeColor = System.Drawing.Color.White
        ТриптофанLabel.Location = New System.Drawing.Point(20, 371)
        ТриптофанLabel.Name = "ТриптофанLabel"
        ТриптофанLabel.Size = New System.Drawing.Size(90, 18)
        ТриптофанLabel.TabIndex = 41
        ТриптофанLabel.Text = "Триптофан:"
        '
        'ВодаLabel
        '
        ВодаLabel.AutoSize = True
        ВодаLabel.ForeColor = System.Drawing.Color.White
        ВодаLabel.Location = New System.Drawing.Point(20, 62)
        ВодаLabel.Name = "ВодаLabel"
        ВодаLabel.Size = New System.Drawing.Size(48, 18)
        ВодаLabel.TabIndex = 19
        ВодаLabel.Text = "Вода:"
        '
        'ЛизинLabel
        '
        ЛизинLabel.AutoSize = True
        ЛизинLabel.ForeColor = System.Drawing.Color.White
        ЛизинLabel.Location = New System.Drawing.Point(20, 397)
        ЛизинLabel.Name = "ЛизинLabel"
        ЛизинLabel.Size = New System.Drawing.Size(55, 18)
        ЛизинLabel.TabIndex = 43
        ЛизинLabel.Text = "Лизин:"
        '
        'ТреонLabel
        '
        ТреонLabel.AutoSize = True
        ТреонLabel.ForeColor = System.Drawing.Color.White
        ТреонLabel.Location = New System.Drawing.Point(20, 423)
        ТреонLabel.Name = "ТреонLabel"
        ТреонLabel.Size = New System.Drawing.Size(54, 18)
        ТреонLabel.TabIndex = 45
        ТреонLabel.Text = "Треон:"
        '
        'Содержание_сухих_веществLabel
        '
        Содержание_сухих_веществLabel.ForeColor = System.Drawing.SystemColors.Window
        Содержание_сухих_веществLabel.Location = New System.Drawing.Point(20, 81)
        Содержание_сухих_веществLabel.Name = "Содержание_сухих_веществLabel"
        Содержание_сухих_веществLabel.Size = New System.Drawing.Size(134, 45)
        Содержание_сухих_веществLabel.TabIndex = 46
        Содержание_сухих_веществLabel.Text = "Содержание сухих веществ:"
        '
        'IngredientsDataGridView
        '
        Me.IngredientsDataGridView.AllowUserToAddRows = False
        Me.IngredientsDataGridView.AllowUserToDeleteRows = False
        Me.IngredientsDataGridView.AllowUserToResizeColumns = False
        Me.IngredientsDataGridView.AllowUserToResizeRows = False
        Me.IngredientsDataGridView.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.IngredientsDataGridView.AutoGenerateColumns = False
        Me.IngredientsDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.IngredientsDataGridView.BackgroundColor = System.Drawing.Color.Gray
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.IngredientsDataGridView.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.IngredientsDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.IngredientsDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Название, Me.Цена, Me.ЭЦ, Me.Белок, Me.Жиры, Me.Углеводы, Me.КодDataGridViewTextBoxColumn, Me.НазваниеDataGridViewTextBoxColumn, Me.ВодаDataGridViewTextBoxColumn, Me.БелокDataGridViewTextBoxColumn, Me.ЖирыDataGridViewTextBoxColumn, Me.УглеводыDataGridViewTextBoxColumn, Me.КлечаткаDataGridViewTextBoxColumn, Me.ЗолаDataGridViewTextBoxColumn, Me.ИзолейцDataGridViewTextBoxColumn, Me.ЛейцDataGridViewTextBoxColumn, Me.ВалинDataGridViewTextBoxColumn, Me.МетЦистDataGridViewTextBoxColumn, Me.ФенТирDataGridViewTextBoxColumn, Me.ТриптофанDataGridViewTextBoxColumn, Me.ЛизинDataGridViewTextBoxColumn, Me.ТреонDataGridViewTextBoxColumn, Me.ЦенаDataGridViewTextBoxColumn, Me.ЭЦDataGridViewTextBoxColumn, Me.СодержаниесухихвеществDataGridViewTextBoxColumn})
        Me.IngredientsDataGridView.DataSource = Me.IngredientsBindingSource
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Monotype Corsiva", 14.25!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        DataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.IngredientsDataGridView.DefaultCellStyle = DataGridViewCellStyle5
        Me.IngredientsDataGridView.Location = New System.Drawing.Point(12, 12)
        Me.IngredientsDataGridView.MultiSelect = False
        Me.IngredientsDataGridView.Name = "IngredientsDataGridView"
        Me.IngredientsDataGridView.ReadOnly = True
        Me.IngredientsDataGridView.RowHeadersVisible = False
        Me.IngredientsDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.IngredientsDataGridView.Size = New System.Drawing.Size(936, 521)
        Me.IngredientsDataGridView.TabIndex = 1
        '
        'Название
        '
        Me.Название.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.Название.DataPropertyName = "Название"
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Arial", 12.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.Название.DefaultCellStyle = DataGridViewCellStyle2
        Me.Название.HeaderText = "Название"
        Me.Название.Name = "Название"
        Me.Название.ReadOnly = True
        Me.Название.Width = 82
        '
        'Цена
        '
        Me.Цена.DataPropertyName = "Цена"
        DataGridViewCellStyle3.BackColor = System.Drawing.Color.Silver
        Me.Цена.DefaultCellStyle = DataGridViewCellStyle3
        Me.Цена.HeaderText = "Цена"
        Me.Цена.Name = "Цена"
        Me.Цена.ReadOnly = True
        '
        'ЭЦ
        '
        Me.ЭЦ.DataPropertyName = "ЭЦ"
        DataGridViewCellStyle4.BackColor = System.Drawing.Color.LightGray
        Me.ЭЦ.DefaultCellStyle = DataGridViewCellStyle4
        Me.ЭЦ.HeaderText = "ЭЦ"
        Me.ЭЦ.Name = "ЭЦ"
        Me.ЭЦ.ReadOnly = True
        '
        'Белок
        '
        Me.Белок.DataPropertyName = "Белок"
        Me.Белок.HeaderText = "Белок"
        Me.Белок.Name = "Белок"
        Me.Белок.ReadOnly = True
        '
        'Жиры
        '
        Me.Жиры.DataPropertyName = "Жиры"
        Me.Жиры.HeaderText = "Жиры"
        Me.Жиры.Name = "Жиры"
        Me.Жиры.ReadOnly = True
        '
        'Углеводы
        '
        Me.Углеводы.DataPropertyName = "Углеводы"
        Me.Углеводы.HeaderText = "Углеводы"
        Me.Углеводы.Name = "Углеводы"
        Me.Углеводы.ReadOnly = True
        '
        'КодDataGridViewTextBoxColumn
        '
        Me.КодDataGridViewTextBoxColumn.DataPropertyName = "Код"
        Me.КодDataGridViewTextBoxColumn.HeaderText = "Код"
        Me.КодDataGridViewTextBoxColumn.Name = "КодDataGridViewTextBoxColumn"
        Me.КодDataGridViewTextBoxColumn.ReadOnly = True
        '
        'НазваниеDataGridViewTextBoxColumn
        '
        Me.НазваниеDataGridViewTextBoxColumn.DataPropertyName = "Название"
        Me.НазваниеDataGridViewTextBoxColumn.HeaderText = "Название"
        Me.НазваниеDataGridViewTextBoxColumn.Name = "НазваниеDataGridViewTextBoxColumn"
        Me.НазваниеDataGridViewTextBoxColumn.ReadOnly = True
        '
        'ВодаDataGridViewTextBoxColumn
        '
        Me.ВодаDataGridViewTextBoxColumn.DataPropertyName = "Вода"
        Me.ВодаDataGridViewTextBoxColumn.HeaderText = "Вода"
        Me.ВодаDataGridViewTextBoxColumn.Name = "ВодаDataGridViewTextBoxColumn"
        Me.ВодаDataGridViewTextBoxColumn.ReadOnly = True
        '
        'БелокDataGridViewTextBoxColumn
        '
        Me.БелокDataGridViewTextBoxColumn.DataPropertyName = "Белок"
        Me.БелокDataGridViewTextBoxColumn.HeaderText = "Белок"
        Me.БелокDataGridViewTextBoxColumn.Name = "БелокDataGridViewTextBoxColumn"
        Me.БелокDataGridViewTextBoxColumn.ReadOnly = True
        '
        'ЖирыDataGridViewTextBoxColumn
        '
        Me.ЖирыDataGridViewTextBoxColumn.DataPropertyName = "Жиры"
        Me.ЖирыDataGridViewTextBoxColumn.HeaderText = "Жиры"
        Me.ЖирыDataGridViewTextBoxColumn.Name = "ЖирыDataGridViewTextBoxColumn"
        Me.ЖирыDataGridViewTextBoxColumn.ReadOnly = True
        '
        'УглеводыDataGridViewTextBoxColumn
        '
        Me.УглеводыDataGridViewTextBoxColumn.DataPropertyName = "Углеводы"
        Me.УглеводыDataGridViewTextBoxColumn.HeaderText = "Углеводы"
        Me.УглеводыDataGridViewTextBoxColumn.Name = "УглеводыDataGridViewTextBoxColumn"
        Me.УглеводыDataGridViewTextBoxColumn.ReadOnly = True
        '
        'КлечаткаDataGridViewTextBoxColumn
        '
        Me.КлечаткаDataGridViewTextBoxColumn.DataPropertyName = "Клечатка"
        Me.КлечаткаDataGridViewTextBoxColumn.HeaderText = "Клечатка"
        Me.КлечаткаDataGridViewTextBoxColumn.Name = "КлечаткаDataGridViewTextBoxColumn"
        Me.КлечаткаDataGridViewTextBoxColumn.ReadOnly = True
        '
        'ЗолаDataGridViewTextBoxColumn
        '
        Me.ЗолаDataGridViewTextBoxColumn.DataPropertyName = "Зола"
        Me.ЗолаDataGridViewTextBoxColumn.HeaderText = "Зола"
        Me.ЗолаDataGridViewTextBoxColumn.Name = "ЗолаDataGridViewTextBoxColumn"
        Me.ЗолаDataGridViewTextBoxColumn.ReadOnly = True
        '
        'ИзолейцDataGridViewTextBoxColumn
        '
        Me.ИзолейцDataGridViewTextBoxColumn.DataPropertyName = "Изолейц"
        Me.ИзолейцDataGridViewTextBoxColumn.HeaderText = "Изолейц"
        Me.ИзолейцDataGridViewTextBoxColumn.Name = "ИзолейцDataGridViewTextBoxColumn"
        Me.ИзолейцDataGridViewTextBoxColumn.ReadOnly = True
        '
        'ЛейцDataGridViewTextBoxColumn
        '
        Me.ЛейцDataGridViewTextBoxColumn.DataPropertyName = "Лейц"
        Me.ЛейцDataGridViewTextBoxColumn.HeaderText = "Лейц"
        Me.ЛейцDataGridViewTextBoxColumn.Name = "ЛейцDataGridViewTextBoxColumn"
        Me.ЛейцDataGridViewTextBoxColumn.ReadOnly = True
        '
        'ВалинDataGridViewTextBoxColumn
        '
        Me.ВалинDataGridViewTextBoxColumn.DataPropertyName = "Валин"
        Me.ВалинDataGridViewTextBoxColumn.HeaderText = "Валин"
        Me.ВалинDataGridViewTextBoxColumn.Name = "ВалинDataGridViewTextBoxColumn"
        Me.ВалинDataGridViewTextBoxColumn.ReadOnly = True
        '
        'МетЦистDataGridViewTextBoxColumn
        '
        Me.МетЦистDataGridViewTextBoxColumn.DataPropertyName = "Мет+Цист"
        Me.МетЦистDataGridViewTextBoxColumn.HeaderText = "Мет+Цист"
        Me.МетЦистDataGridViewTextBoxColumn.Name = "МетЦистDataGridViewTextBoxColumn"
        Me.МетЦистDataGridViewTextBoxColumn.ReadOnly = True
        '
        'ФенТирDataGridViewTextBoxColumn
        '
        Me.ФенТирDataGridViewTextBoxColumn.DataPropertyName = "Фен+Тир"
        Me.ФенТирDataGridViewTextBoxColumn.HeaderText = "Фен+Тир"
        Me.ФенТирDataGridViewTextBoxColumn.Name = "ФенТирDataGridViewTextBoxColumn"
        Me.ФенТирDataGridViewTextBoxColumn.ReadOnly = True
        '
        'ТриптофанDataGridViewTextBoxColumn
        '
        Me.ТриптофанDataGridViewTextBoxColumn.DataPropertyName = "Триптофан"
        Me.ТриптофанDataGridViewTextBoxColumn.HeaderText = "Триптофан"
        Me.ТриптофанDataGridViewTextBoxColumn.Name = "ТриптофанDataGridViewTextBoxColumn"
        Me.ТриптофанDataGridViewTextBoxColumn.ReadOnly = True
        '
        'ЛизинDataGridViewTextBoxColumn
        '
        Me.ЛизинDataGridViewTextBoxColumn.DataPropertyName = "Лизин"
        Me.ЛизинDataGridViewTextBoxColumn.HeaderText = "Лизин"
        Me.ЛизинDataGridViewTextBoxColumn.Name = "ЛизинDataGridViewTextBoxColumn"
        Me.ЛизинDataGridViewTextBoxColumn.ReadOnly = True
        '
        'ТреонDataGridViewTextBoxColumn
        '
        Me.ТреонDataGridViewTextBoxColumn.DataPropertyName = "Треон"
        Me.ТреонDataGridViewTextBoxColumn.HeaderText = "Треон"
        Me.ТреонDataGridViewTextBoxColumn.Name = "ТреонDataGridViewTextBoxColumn"
        Me.ТреонDataGridViewTextBoxColumn.ReadOnly = True
        '
        'ЦенаDataGridViewTextBoxColumn
        '
        Me.ЦенаDataGridViewTextBoxColumn.DataPropertyName = "Цена"
        Me.ЦенаDataGridViewTextBoxColumn.HeaderText = "Цена"
        Me.ЦенаDataGridViewTextBoxColumn.Name = "ЦенаDataGridViewTextBoxColumn"
        Me.ЦенаDataGridViewTextBoxColumn.ReadOnly = True
        '
        'ЭЦDataGridViewTextBoxColumn
        '
        Me.ЭЦDataGridViewTextBoxColumn.DataPropertyName = "ЭЦ"
        Me.ЭЦDataGridViewTextBoxColumn.HeaderText = "ЭЦ"
        Me.ЭЦDataGridViewTextBoxColumn.Name = "ЭЦDataGridViewTextBoxColumn"
        Me.ЭЦDataGridViewTextBoxColumn.ReadOnly = True
        '
        'СодержаниесухихвеществDataGridViewTextBoxColumn
        '
        Me.СодержаниесухихвеществDataGridViewTextBoxColumn.DataPropertyName = "Содержание_сухих_веществ"
        Me.СодержаниесухихвеществDataGridViewTextBoxColumn.HeaderText = "Содержание_сухих_веществ"
        Me.СодержаниесухихвеществDataGridViewTextBoxColumn.Name = "СодержаниесухихвеществDataGridViewTextBoxColumn"
        Me.СодержаниесухихвеществDataGridViewTextBoxColumn.ReadOnly = True
        '
        'IngredientsBindingSource
        '
        Me.IngredientsBindingSource.DataMember = "Ingredients"
        Me.IngredientsBindingSource.DataSource = Me.ProductsDBDataSet
        '
        'ProductsDBDataSet
        '
        Me.ProductsDBDataSet.DataSetName = "ProductsDBDataSet"
        Me.ProductsDBDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'btnOptim
        '
        Me.btnOptim.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnOptim.BackColor = System.Drawing.Color.DodgerBlue
        Me.btnOptim.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.btnOptim.FlatAppearance.BorderColor = System.Drawing.Color.DeepSkyBlue
        Me.btnOptim.FlatAppearance.BorderSize = 0
        Me.btnOptim.FlatAppearance.MouseDownBackColor = System.Drawing.Color.SteelBlue
        Me.btnOptim.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DeepSkyBlue
        Me.btnOptim.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnOptim.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.btnOptim.ForeColor = System.Drawing.Color.White
        Me.btnOptim.Image = CType(resources.GetObject("btnOptim.Image"), System.Drawing.Image)
        Me.btnOptim.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnOptim.Location = New System.Drawing.Point(972, 12)
        Me.btnOptim.Name = "btnOptim"
        Me.btnOptim.Size = New System.Drawing.Size(190, 56)
        Me.btnOptim.TabIndex = 14
        Me.btnOptim.Text = "Добавить новый ингредиент"
        Me.btnOptim.UseVisualStyleBackColor = False
        '
        'Button3
        '
        Me.Button3.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button3.BackColor = System.Drawing.Color.DodgerBlue
        Me.Button3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.Button3.FlatAppearance.BorderColor = System.Drawing.Color.DeepSkyBlue
        Me.Button3.FlatAppearance.BorderSize = 0
        Me.Button3.FlatAppearance.MouseDownBackColor = System.Drawing.Color.SteelBlue
        Me.Button3.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DeepSkyBlue
        Me.Button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.Button3.ForeColor = System.Drawing.Color.White
        Me.Button3.Image = CType(resources.GetObject("Button3.Image"), System.Drawing.Image)
        Me.Button3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button3.Location = New System.Drawing.Point(1168, 12)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(190, 56)
        Me.Button3.TabIndex = 15
        Me.Button3.Text = "Удалить ингредиент"
        Me.Button3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Button3.UseVisualStyleBackColor = False
        '
        'IngredientsTableAdapter
        '
        Me.IngredientsTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.IngredientsTableAdapter = Me.IngredientsTableAdapter
        Me.TableAdapterManager.IngTableAdapter = Nothing
        Me.TableAdapterManager.RecipesTableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = ProjectUMeat.ProductsDBDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'GroupBox4
        '
        Me.GroupBox4.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox4.Controls.Add(КодLabel)
        Me.GroupBox4.Controls.Add(Me.КодLabel1)
        Me.GroupBox4.Controls.Add(Содержание_сухих_веществLabel)
        Me.GroupBox4.Controls.Add(Me.ТреонTextBox)
        Me.GroupBox4.Controls.Add(Me.Содержание_сухих_веществTextBox)
        Me.GroupBox4.Controls.Add(ТреонLabel)
        Me.GroupBox4.Controls.Add(Me.ЛизинTextBox)
        Me.GroupBox4.Controls.Add(ЛизинLabel)
        Me.GroupBox4.Controls.Add(ВодаLabel)
        Me.GroupBox4.Controls.Add(Me.ТриптофанTextBox)
        Me.GroupBox4.Controls.Add(Me.ВодаTextBox)
        Me.GroupBox4.Controls.Add(ТриптофанLabel)
        Me.GroupBox4.Controls.Add(КлечаткаLabel)
        Me.GroupBox4.Controls.Add(Me.Фен_ТирTextBox)
        Me.GroupBox4.Controls.Add(Me.КлечаткаTextBox)
        Me.GroupBox4.Controls.Add(Фен_ТирLabel)
        Me.GroupBox4.Controls.Add(ЗолаLabel)
        Me.GroupBox4.Controls.Add(Me.Мет_ЦистTextBox)
        Me.GroupBox4.Controls.Add(Me.ЗолаTextBox)
        Me.GroupBox4.Controls.Add(Мет_ЦистLabel)
        Me.GroupBox4.Controls.Add(ИзолейцLabel)
        Me.GroupBox4.Controls.Add(Me.ВалинTextBox)
        Me.GroupBox4.Controls.Add(Me.ИзолейцTextBox)
        Me.GroupBox4.Controls.Add(ВалинLabel)
        Me.GroupBox4.Controls.Add(ЛейцLabel)
        Me.GroupBox4.Controls.Add(Me.ЛейцTextBox)
        Me.GroupBox4.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.GroupBox4.ForeColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.GroupBox4.Location = New System.Drawing.Point(973, 91)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(385, 467)
        Me.GroupBox4.TabIndex = 48
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Общие показатели ингредиента"
        '
        'КодLabel1
        '
        Me.КодLabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.IngredientsBindingSource, "Код", True))
        Me.КодLabel1.ForeColor = System.Drawing.Color.White
        Me.КодLabel1.Location = New System.Drawing.Point(54, 23)
        Me.КодLabel1.Name = "КодLabel1"
        Me.КодLabel1.Size = New System.Drawing.Size(100, 23)
        Me.КодLabel1.TabIndex = 48
        Me.КодLabel1.Text = "Label1"
        '
        'ТреонTextBox
        '
        Me.ТреонTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.IngredientsBindingSource, "Треон", True))
        Me.ТреонTextBox.Location = New System.Drawing.Point(172, 420)
        Me.ТреонTextBox.Name = "ТреонTextBox"
        Me.ТреонTextBox.Size = New System.Drawing.Size(100, 24)
        Me.ТреонTextBox.TabIndex = 46
        '
        'Содержание_сухих_веществTextBox
        '
        Me.Содержание_сухих_веществTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.IngredientsBindingSource, "Содержание_сухих_веществ", True))
        Me.Содержание_сухих_веществTextBox.Location = New System.Drawing.Point(172, 59)
        Me.Содержание_сухих_веществTextBox.Name = "Содержание_сухих_веществTextBox"
        Me.Содержание_сухих_веществTextBox.Size = New System.Drawing.Size(100, 24)
        Me.Содержание_сухих_веществTextBox.TabIndex = 47
        '
        'ЛизинTextBox
        '
        Me.ЛизинTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.IngredientsBindingSource, "Лизин", True))
        Me.ЛизинTextBox.Location = New System.Drawing.Point(172, 394)
        Me.ЛизинTextBox.Name = "ЛизинTextBox"
        Me.ЛизинTextBox.Size = New System.Drawing.Size(100, 24)
        Me.ЛизинTextBox.TabIndex = 44
        '
        'ТриптофанTextBox
        '
        Me.ТриптофанTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.IngredientsBindingSource, "Триптофан", True))
        Me.ТриптофанTextBox.Location = New System.Drawing.Point(172, 368)
        Me.ТриптофанTextBox.Name = "ТриптофанTextBox"
        Me.ТриптофанTextBox.Size = New System.Drawing.Size(100, 24)
        Me.ТриптофанTextBox.TabIndex = 42
        '
        'ВодаTextBox
        '
        Me.ВодаTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.IngredientsBindingSource, "Вода", True))
        Me.ВодаTextBox.Location = New System.Drawing.Point(172, 86)
        Me.ВодаTextBox.Name = "ВодаTextBox"
        Me.ВодаTextBox.Size = New System.Drawing.Size(100, 24)
        Me.ВодаTextBox.TabIndex = 20
        '
        'Фен_ТирTextBox
        '
        Me.Фен_ТирTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.IngredientsBindingSource, "Фен+Тир", True))
        Me.Фен_ТирTextBox.Location = New System.Drawing.Point(172, 342)
        Me.Фен_ТирTextBox.Name = "Фен_ТирTextBox"
        Me.Фен_ТирTextBox.Size = New System.Drawing.Size(100, 24)
        Me.Фен_ТирTextBox.TabIndex = 40
        '
        'КлечаткаTextBox
        '
        Me.КлечаткаTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.IngredientsBindingSource, "Клечатка", True))
        Me.КлечаткаTextBox.Location = New System.Drawing.Point(172, 147)
        Me.КлечаткаTextBox.Name = "КлечаткаTextBox"
        Me.КлечаткаTextBox.Size = New System.Drawing.Size(100, 24)
        Me.КлечаткаTextBox.TabIndex = 28
        '
        'Мет_ЦистTextBox
        '
        Me.Мет_ЦистTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.IngredientsBindingSource, "Мет+Цист", True))
        Me.Мет_ЦистTextBox.Location = New System.Drawing.Point(172, 316)
        Me.Мет_ЦистTextBox.Name = "Мет_ЦистTextBox"
        Me.Мет_ЦистTextBox.Size = New System.Drawing.Size(100, 24)
        Me.Мет_ЦистTextBox.TabIndex = 38
        '
        'ЗолаTextBox
        '
        Me.ЗолаTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.IngredientsBindingSource, "Зола", True))
        Me.ЗолаTextBox.Location = New System.Drawing.Point(172, 173)
        Me.ЗолаTextBox.Name = "ЗолаTextBox"
        Me.ЗолаTextBox.Size = New System.Drawing.Size(100, 24)
        Me.ЗолаTextBox.TabIndex = 30
        '
        'ВалинTextBox
        '
        Me.ВалинTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.IngredientsBindingSource, "Валин", True))
        Me.ВалинTextBox.Location = New System.Drawing.Point(172, 290)
        Me.ВалинTextBox.Name = "ВалинTextBox"
        Me.ВалинTextBox.Size = New System.Drawing.Size(100, 24)
        Me.ВалинTextBox.TabIndex = 36
        '
        'ИзолейцTextBox
        '
        Me.ИзолейцTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.IngredientsBindingSource, "Изолейц", True))
        Me.ИзолейцTextBox.Location = New System.Drawing.Point(172, 237)
        Me.ИзолейцTextBox.Name = "ИзолейцTextBox"
        Me.ИзолейцTextBox.Size = New System.Drawing.Size(100, 24)
        Me.ИзолейцTextBox.TabIndex = 32
        '
        'ЛейцTextBox
        '
        Me.ЛейцTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.IngredientsBindingSource, "Лейц", True))
        Me.ЛейцTextBox.Location = New System.Drawing.Point(172, 264)
        Me.ЛейцTextBox.Name = "ЛейцTextBox"
        Me.ЛейцTextBox.Size = New System.Drawing.Size(100, 24)
        Me.ЛейцTextBox.TabIndex = 34
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.Label9.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Label9.Location = New System.Drawing.Point(12, 540)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(257, 18)
        Me.Label9.TabIndex = 49
        Me.Label9.Text = "Авторы ПО: Кимсанбаев Карим"
        '
        'frmEditorIngredients
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.BackColor = System.Drawing.Color.DimGray
        Me.ClientSize = New System.Drawing.Size(1370, 578)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.GroupBox4)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.btnOptim)
        Me.Controls.Add(Me.IngredientsDataGridView)
        Me.Name = "frmEditorIngredients"
        Me.Text = "Редактор ингредиентов"
        CType(Me.IngredientsDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.IngredientsBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ProductsDBDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents ProductsDBDataSet As ProductsDBDataSet
    Friend WithEvents IngredientsBindingSource As BindingSource
    Friend WithEvents IngredientsTableAdapter As ProductsDBDataSetTableAdapters.IngredientsTableAdapter
    Friend WithEvents TableAdapterManager As ProductsDBDataSetTableAdapters.TableAdapterManager
    Friend WithEvents IngredientsDataGridView As DataGridView
    Friend WithEvents btnOptim As Button
    Friend WithEvents Button3 As Button
    Friend WithEvents GroupBox4 As GroupBox
    Friend WithEvents КодLabel1 As Label
    Friend WithEvents ТреонTextBox As TextBox
    Friend WithEvents Содержание_сухих_веществTextBox As TextBox
    Friend WithEvents ЛизинTextBox As TextBox
    Friend WithEvents ТриптофанTextBox As TextBox
    Friend WithEvents ВодаTextBox As TextBox
    Friend WithEvents Фен_ТирTextBox As TextBox
    Friend WithEvents КлечаткаTextBox As TextBox
    Friend WithEvents Мет_ЦистTextBox As TextBox
    Friend WithEvents ЗолаTextBox As TextBox
    Friend WithEvents ВалинTextBox As TextBox
    Friend WithEvents ИзолейцTextBox As TextBox
    Friend WithEvents ЛейцTextBox As TextBox
    Friend WithEvents Название As DataGridViewTextBoxColumn
    Friend WithEvents Цена As DataGridViewTextBoxColumn
    Friend WithEvents ЭЦ As DataGridViewTextBoxColumn
    Friend WithEvents Белок As DataGridViewTextBoxColumn
    Friend WithEvents Жиры As DataGridViewTextBoxColumn
    Friend WithEvents Углеводы As DataGridViewTextBoxColumn
    Friend WithEvents КодDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents НазваниеDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents ВодаDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents БелокDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents ЖирыDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents УглеводыDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents КлечаткаDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents ЗолаDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents ИзолейцDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents ЛейцDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents ВалинDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents МетЦистDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents ФенТирDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents ТриптофанDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents ЛизинDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents ТреонDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents ЦенаDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents ЭЦDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents СодержаниесухихвеществDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents Label9 As Label
End Class

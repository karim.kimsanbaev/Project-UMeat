﻿Public Class frmEditorIngredients
    Private Sub frmEditorIngredients_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            Me.IngredientsTableAdapter.Fill(Me.ProductsDBDataSet.Ingredients)
        Catch ex As Exception
            MsgBox("База данных не подключена")
            Application.Exit()
        End Try
    End Sub
    Private Sub DeleteIngredient(sender As Object, e As EventArgs)
        Try
            Try
                Validate()
                TableAdapterManager.UpdateAll(ProductsDBDataSet)
            Catch ex As Exception
                MsgBox("Ошибка удаления")
                Return
            End Try
            MessageBox.Show("Сохранение успешно", "Сохранение", MessageBoxButtons.OK, MessageBoxIcon.Asterisk)
        Catch ex As Exception
            MessageBox.Show("Не возможно удалить ингредиент. Ошибка связи с базой данных!", "Удаление из Базы Данных", MessageBoxButtons.OK, MessageBoxIcon.Stop)
        End Try
        MessageBox.Show("Ингредиент успешно удалён", "Удаление из Базы Данных", MessageBoxButtons.OK, MessageBoxIcon.Information)
    End Sub
    Private Sub Button2_Click(sender As Object, e As EventArgs)
        Dim form As New AddIngredientToDB
        form.ShowDialog()
        Me.IngredientsTableAdapter.Fill(Me.ProductsDBDataSet.Ingredients)
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs)
        Dim res% = MessageBox.Show("Вы действительно хотите дулаить ингредиент из Базы Данных?", "Удаление из Базы Данных", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
        If res = 7 Then
            Me.IngredientsTableAdapter.Fill(Me.ProductsDBDataSet.Ingredients)
        Else

            DeleteIngredient(sender, e)
        End If
    End Sub

    Private Sub frmEditorIngredients_Deactivate(sender As Object, e As EventArgs) Handles MyBase.Deactivate
        Try
            Validate()
            IngredientsBindingSource.EndEdit()
            TableAdapterManager.UpdateAll(ProductsDBDataSet)
        Catch ex As Exception
            Return
        End Try
    End Sub

    Private Sub IngredientsDataGridView_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles IngredientsDataGridView.CellContentClick

    End Sub

    Private Sub frmEditorIngredients_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        Главное_меню.Show()
    End Sub

    Private Sub btnOptim_Click(sender As Object, e As EventArgs) Handles btnOptim.Click
        Dim form As New AddIngredientToDB
        form.ShowDialog()
        Me.IngredientsTableAdapter.Fill(Me.ProductsDBDataSet.Ingredients)
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        Dim res% = MessageBox.Show("Вы действительно хотите удалить ингредиент из Базы Данных?", "Удаление из Базы Данных", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
        If res = 7 Then
            Me.IngredientsTableAdapter.Fill(Me.ProductsDBDataSet.Ingredients)
        Else
            Dim r As DataRowView
            'ProductsDBDataSet.Ingredients.RemoveIngredientsRow()
            r = IngredientsBindingSource.Current()
            ProductsDBDataSet.Ingredients.RemoveIngredientsRow(ProductsDBDataSet.Ingredients.FindByКод(r.Item(0)))
            MsgBox(1)
            'IngredientsBindingSource.ResetAllowNew()
            Return
            DeleteIngredient(sender, e)
        End If
    End Sub
End Class
﻿Module Расчёт_Липатова_Рогова
    Structure mainComponents
        Dim Белки As Double
        Dim Жиры As Double
        Dim Углеводы As Double
    End Structure
    Dim test As mainComponents
    Dim dgv As DataGridView
    Public Sub SetDataGridView(dgvOther As DataGridView)
        dgv = dgvOther
    End Sub

    Public Function РасчитатьОсновнойКомпонент(indColumn%) As Double
        Dim доляКомпонента As Double = 0
        Dim суммаДоли As Double = 0
        For i% = 0 To dgv.RowCount - 1
            доляКомпонента = доляКомпонента + dgv.Rows(i).Cells(1).Value * dgv.Rows(i).Cells(indColumn).Value
            суммаДоли = суммаДоли + dgv.Rows(i).Cells(1).Value
        Next
        Return доляКомпонента / суммаДоли
    End Function

    Public Function РасчитатьЭЦ_ккал(row As DataGridViewRow) As Integer
        Return row.Cells(0).Value * 4 + row.Cells(1).Value * 9 + row.Cells(2).Value * 4
    End Function

    Public Function РасчитатьЭЦ_кДж(row As DataGridViewRow) As Integer
        Return row.Cells(0).Value * 17 + row.Cells(1).Value * 37 + row.Cells(2).Value * 4
    End Function
    Dim мд_белок As Double = 0
    Public Function Расчитать_М(indАминокислота As Integer) As Double
        If мд_белок = 0 Then
            For i% = 0 To dgv.RowCount - 1
                мд_белок = мд_белок + dgv.Rows(i).Cells(1).Value * dgv.Rows(i).Cells(3).Value
            Next
        End If

        Dim мд_белок_аминокислота As Double = 0
        For i% = 0 To dgv.RowCount - 1
            мд_белок_аминокислота = мд_белок_аминокислота + dgv.Rows(i).Cells(1).Value * dgv.Rows(i).Cells(3).Value * dgv.Rows(i).Cells(indАминокислота).Value
        Next

        Return мд_белок_аминокислота / мд_белок
    End Function
    Dim Cmin As Double
    Public Function Расчитать_Cmin(row As DataGridViewRow, cntColumn%) As Double
        Dim min As Double
        min = row.Cells(1).Value
        For i% = 2 To cntColumn - 1
            min = Math.Min(min, row.Cells(i).Value)
        Next
        Cmin = min
        Return min
    End Function

    Public Function Расчитать_КРАС(row As DataGridViewRow, cntColumn%) As Double
        Dim КРАС As Double = 0
        For i% = 1 To cntColumn - 1
            КРАС = КРАС + row.Cells(i).Value - Cmin
        Next
        Return КРАС / 8
    End Function
    Private Function СуммаАминокислот(indColumn%) As Double
        Dim sum As Double = 0
        For i% = 0 To dgv.RowCount - 1
            sum = sum + dgv.Rows(i).Cells(indColumn).Value
        Next
        Return sum
    End Function
    Public Function Расчитать_U(row As DataGridViewRow, cntColumn%) As Double
        Dim sumU As Double = 0
        For i% = 1 To cntColumn - 1
            sumU = sumU + row.Cells(i).Value * СуммаАминокислот(i + 7)
        Next
        Return sumU / СуммаАминокислот(8)
    End Function
    Public Function Расчитать_G(row As DataGridViewRow, cntColumn%) As Double
        Dim sumG As Double = 0
        For i% = 1 To cntColumn - 1
            sumG = sumG + (1 - row.Cells(i).Value) * СуммаАминокислот(i + 7)
        Next
        Return sumG / Cmin
    End Function
End Module

﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEditorRecipe
    Inherits System.Windows.Forms.Form

    'Форма переопределяет dispose для очистки списка компонентов.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Является обязательной для конструктора форм Windows Forms
    Private components As System.ComponentModel.IContainer

    'Примечание: следующая процедура является обязательной для конструктора форм Windows Forms
    'Для ее изменения используйте конструктор форм Windows Form.  
    'Не изменяйте ее в редакторе исходного кода.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim КодLabel As System.Windows.Forms.Label
        Dim Содержание_сухих_веществLabel As System.Windows.Forms.Label
        Dim ТреонLabel As System.Windows.Forms.Label
        Dim ЛизинLabel As System.Windows.Forms.Label
        Dim ВодаLabel As System.Windows.Forms.Label
        Dim ТриптофанLabel As System.Windows.Forms.Label
        Dim КлечаткаLabel As System.Windows.Forms.Label
        Dim Фен_ТирLabel As System.Windows.Forms.Label
        Dim ЗолаLabel As System.Windows.Forms.Label
        Dim Мет_ЦистLabel As System.Windows.Forms.Label
        Dim ИзолейцLabel As System.Windows.Forms.Label
        Dim ВалинLabel As System.Windows.Forms.Label
        Dim ЛейцLabel As System.Windows.Forms.Label
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEditorRecipe))
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle13 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.dgvRecipe1 = New System.Windows.Forms.DataGridView()
        Me.Название_ингредиента = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Массовая_доля = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Белки = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Жиры = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Углеводы = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Цена = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ЭЦ = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.КодLabel1 = New System.Windows.Forms.Label()
        Me.ТреонTextBox = New System.Windows.Forms.TextBox()
        Me.Содержание_сухих_веществTextBox = New System.Windows.Forms.TextBox()
        Me.ЛизинTextBox = New System.Windows.Forms.TextBox()
        Me.ТриптофанTextBox = New System.Windows.Forms.TextBox()
        Me.ВодаTextBox = New System.Windows.Forms.TextBox()
        Me.Фен_ТирTextBox = New System.Windows.Forms.TextBox()
        Me.КлечаткаTextBox = New System.Windows.Forms.TextBox()
        Me.Мет_ЦистTextBox = New System.Windows.Forms.TextBox()
        Me.ЗолаTextBox = New System.Windows.Forms.TextBox()
        Me.ВалинTextBox = New System.Windows.Forms.TextBox()
        Me.ИзолейцTextBox = New System.Windows.Forms.TextBox()
        Me.ЛейцTextBox = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.CodeRecipe = New System.Windows.Forms.Label()
        Me.ProductsDBDataSet = New ProjectUMeat.ProductsDBDataSet()
        Me.IngredientsBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.IngredientsTableAdapter = New ProjectUMeat.ProductsDBDataSetTableAdapters.IngredientsTableAdapter()
        Me.TableAdapterManager = New ProjectUMeat.ProductsDBDataSetTableAdapters.TableAdapterManager()
        Me.НазваниеListBox = New System.Windows.Forms.ListBox()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.dgvОсновныеКомпоненты = New System.Windows.Forms.DataGridView()
        Me.БелкиPer = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ЖирыPer = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.УглеводыPer = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgvЭнергетическаяЦенность = New System.Windows.Forms.DataGridView()
        Me.кКал = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.кДж = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgvНак_Скор = New System.Windows.Forms.DataGridView()
        Me.Column1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column9 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column7 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column8 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgvБелковыйКомпонент = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column10 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtNameRecipe = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.btnOptim = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Label9 = New System.Windows.Forms.Label()
        КодLabel = New System.Windows.Forms.Label()
        Содержание_сухих_веществLabel = New System.Windows.Forms.Label()
        ТреонLabel = New System.Windows.Forms.Label()
        ЛизинLabel = New System.Windows.Forms.Label()
        ВодаLabel = New System.Windows.Forms.Label()
        ТриптофанLabel = New System.Windows.Forms.Label()
        КлечаткаLabel = New System.Windows.Forms.Label()
        Фен_ТирLabel = New System.Windows.Forms.Label()
        ЗолаLabel = New System.Windows.Forms.Label()
        Мет_ЦистLabel = New System.Windows.Forms.Label()
        ИзолейцLabel = New System.Windows.Forms.Label()
        ВалинLabel = New System.Windows.Forms.Label()
        ЛейцLabel = New System.Windows.Forms.Label()
        CType(Me.dgvRecipe1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox4.SuspendLayout()
        CType(Me.ProductsDBDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.IngredientsBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        CType(Me.dgvОсновныеКомпоненты, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvЭнергетическаяЦенность, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvНак_Скор, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvБелковыйКомпонент, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'КодLabel
        '
        КодLabel.AutoSize = True
        КодLabel.ForeColor = System.Drawing.Color.White
        КодLabel.Location = New System.Drawing.Point(6, 23)
        КодLabel.Name = "КодLabel"
        КодLabel.Size = New System.Drawing.Size(40, 18)
        КодLabel.TabIndex = 47
        КодLabel.Text = "Код:"
        '
        'Содержание_сухих_веществLabel
        '
        Содержание_сухих_веществLabel.ForeColor = System.Drawing.SystemColors.Window
        Содержание_сухих_веществLabel.Location = New System.Drawing.Point(20, 81)
        Содержание_сухих_веществLabel.Name = "Содержание_сухих_веществLabel"
        Содержание_сухих_веществLabel.Size = New System.Drawing.Size(134, 45)
        Содержание_сухих_веществLabel.TabIndex = 46
        Содержание_сухих_веществLabel.Text = "Содержание сухих веществ:"
        '
        'ТреонLabel
        '
        ТреонLabel.AutoSize = True
        ТреонLabel.ForeColor = System.Drawing.Color.White
        ТреонLabel.Location = New System.Drawing.Point(20, 423)
        ТреонLabel.Name = "ТреонLabel"
        ТреонLabel.Size = New System.Drawing.Size(54, 18)
        ТреонLabel.TabIndex = 45
        ТреонLabel.Text = "Треон:"
        '
        'ЛизинLabel
        '
        ЛизинLabel.AutoSize = True
        ЛизинLabel.ForeColor = System.Drawing.Color.White
        ЛизинLabel.Location = New System.Drawing.Point(20, 397)
        ЛизинLabel.Name = "ЛизинLabel"
        ЛизинLabel.Size = New System.Drawing.Size(55, 18)
        ЛизинLabel.TabIndex = 43
        ЛизинLabel.Text = "Лизин:"
        '
        'ВодаLabel
        '
        ВодаLabel.AutoSize = True
        ВодаLabel.ForeColor = System.Drawing.Color.White
        ВодаLabel.Location = New System.Drawing.Point(20, 62)
        ВодаLabel.Name = "ВодаLabel"
        ВодаLabel.Size = New System.Drawing.Size(48, 18)
        ВодаLabel.TabIndex = 19
        ВодаLabel.Text = "Вода:"
        '
        'ТриптофанLabel
        '
        ТриптофанLabel.AutoSize = True
        ТриптофанLabel.ForeColor = System.Drawing.Color.White
        ТриптофанLabel.Location = New System.Drawing.Point(20, 371)
        ТриптофанLabel.Name = "ТриптофанLabel"
        ТриптофанLabel.Size = New System.Drawing.Size(90, 18)
        ТриптофанLabel.TabIndex = 41
        ТриптофанLabel.Text = "Триптофан:"
        '
        'КлечаткаLabel
        '
        КлечаткаLabel.AutoSize = True
        КлечаткаLabel.ForeColor = System.Drawing.Color.White
        КлечаткаLabel.Location = New System.Drawing.Point(20, 150)
        КлечаткаLabel.Name = "КлечаткаLabel"
        КлечаткаLabel.Size = New System.Drawing.Size(78, 18)
        КлечаткаLabel.TabIndex = 27
        КлечаткаLabel.Text = "Клечатка:"
        '
        'Фен_ТирLabel
        '
        Фен_ТирLabel.AutoSize = True
        Фен_ТирLabel.ForeColor = System.Drawing.Color.White
        Фен_ТирLabel.Location = New System.Drawing.Point(20, 345)
        Фен_ТирLabel.Name = "Фен_ТирLabel"
        Фен_ТирLabel.Size = New System.Drawing.Size(75, 18)
        Фен_ТирLabel.TabIndex = 39
        Фен_ТирLabel.Text = "Фен+Тир:"
        '
        'ЗолаLabel
        '
        ЗолаLabel.AutoSize = True
        ЗолаLabel.ForeColor = System.Drawing.Color.White
        ЗолаLabel.Location = New System.Drawing.Point(20, 180)
        ЗолаLabel.Name = "ЗолаLabel"
        ЗолаLabel.Size = New System.Drawing.Size(48, 18)
        ЗолаLabel.TabIndex = 29
        ЗолаLabel.Text = "Зола:"
        '
        'Мет_ЦистLabel
        '
        Мет_ЦистLabel.AutoSize = True
        Мет_ЦистLabel.ForeColor = System.Drawing.Color.White
        Мет_ЦистLabel.Location = New System.Drawing.Point(20, 319)
        Мет_ЦистLabel.Name = "Мет_ЦистLabel"
        Мет_ЦистLabel.Size = New System.Drawing.Size(83, 18)
        Мет_ЦистLabel.TabIndex = 37
        Мет_ЦистLabel.Text = "Мет+Цист:"
        '
        'ИзолейцLabel
        '
        ИзолейцLabel.AutoSize = True
        ИзолейцLabel.ForeColor = System.Drawing.Color.White
        ИзолейцLabel.Location = New System.Drawing.Point(20, 245)
        ИзолейцLabel.Name = "ИзолейцLabel"
        ИзолейцLabel.Size = New System.Drawing.Size(73, 18)
        ИзолейцLabel.TabIndex = 31
        ИзолейцLabel.Text = "Изолейц:"
        '
        'ВалинLabel
        '
        ВалинLabel.AutoSize = True
        ВалинLabel.ForeColor = System.Drawing.Color.White
        ВалинLabel.Location = New System.Drawing.Point(20, 293)
        ВалинLabel.Name = "ВалинLabel"
        ВалинLabel.Size = New System.Drawing.Size(55, 18)
        ВалинLabel.TabIndex = 35
        ВалинLabel.Text = "Валин:"
        '
        'ЛейцLabel
        '
        ЛейцLabel.AutoSize = True
        ЛейцLabel.ForeColor = System.Drawing.Color.White
        ЛейцLabel.Location = New System.Drawing.Point(20, 267)
        ЛейцLabel.Name = "ЛейцLabel"
        ЛейцLabel.Size = New System.Drawing.Size(47, 18)
        ЛейцLabel.TabIndex = 33
        ЛейцLabel.Text = "Лейц:"
        '
        'dgvRecipe1
        '
        Me.dgvRecipe1.AllowUserToAddRows = False
        Me.dgvRecipe1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvRecipe1.BackgroundColor = System.Drawing.Color.Gray
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.ControlDarkDark
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvRecipe1.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvRecipe1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvRecipe1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Название_ингредиента, Me.Массовая_доля, Me.Белки, Me.Жиры, Me.Углеводы, Me.Цена, Me.ЭЦ})
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle5.BackColor = System.Drawing.Color.White
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Monotype Corsiva", 14.25!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        DataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvRecipe1.DefaultCellStyle = DataGridViewCellStyle5
        Me.dgvRecipe1.Location = New System.Drawing.Point(347, 85)
        Me.dgvRecipe1.Name = "dgvRecipe1"
        Me.dgvRecipe1.RowHeadersVisible = False
        Me.dgvRecipe1.Size = New System.Drawing.Size(784, 279)
        Me.dgvRecipe1.TabIndex = 0
        '
        'Название_ингредиента
        '
        Me.Название_ингредиента.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Arial", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.Название_ингредиента.DefaultCellStyle = DataGridViewCellStyle2
        Me.Название_ингредиента.HeaderText = "Название ингредиента"
        Me.Название_ингредиента.Name = "Название_ингредиента"
        Me.Название_ингредиента.ReadOnly = True
        Me.Название_ингредиента.Width = 136
        '
        'Массовая_доля
        '
        Me.Массовая_доля.HeaderText = "Массовая доля"
        Me.Массовая_доля.Name = "Массовая_доля"
        '
        'Белки
        '
        Me.Белки.HeaderText = "Белки"
        Me.Белки.Name = "Белки"
        Me.Белки.ReadOnly = True
        '
        'Жиры
        '
        Me.Жиры.HeaderText = "Жиры"
        Me.Жиры.Name = "Жиры"
        Me.Жиры.ReadOnly = True
        '
        'Углеводы
        '
        Me.Углеводы.HeaderText = "Углеводы"
        Me.Углеводы.Name = "Углеводы"
        Me.Углеводы.ReadOnly = True
        '
        'Цена
        '
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle3.BackColor = System.Drawing.Color.Silver
        Me.Цена.DefaultCellStyle = DataGridViewCellStyle3
        Me.Цена.HeaderText = "Цена за 1 кг"
        Me.Цена.Name = "Цена"
        Me.Цена.ReadOnly = True
        '
        'ЭЦ
        '
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle4.BackColor = System.Drawing.Color.LightGray
        Me.ЭЦ.DefaultCellStyle = DataGridViewCellStyle4
        Me.ЭЦ.HeaderText = "Энергетическая ценность"
        Me.ЭЦ.Name = "ЭЦ"
        Me.ЭЦ.ReadOnly = True
        '
        'GroupBox4
        '
        Me.GroupBox4.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox4.Controls.Add(КодLabel)
        Me.GroupBox4.Controls.Add(Me.КодLabel1)
        Me.GroupBox4.Controls.Add(Содержание_сухих_веществLabel)
        Me.GroupBox4.Controls.Add(Me.ТреонTextBox)
        Me.GroupBox4.Controls.Add(Me.Содержание_сухих_веществTextBox)
        Me.GroupBox4.Controls.Add(ТреонLabel)
        Me.GroupBox4.Controls.Add(Me.ЛизинTextBox)
        Me.GroupBox4.Controls.Add(ЛизинLabel)
        Me.GroupBox4.Controls.Add(ВодаLabel)
        Me.GroupBox4.Controls.Add(Me.ТриптофанTextBox)
        Me.GroupBox4.Controls.Add(Me.ВодаTextBox)
        Me.GroupBox4.Controls.Add(ТриптофанLabel)
        Me.GroupBox4.Controls.Add(КлечаткаLabel)
        Me.GroupBox4.Controls.Add(Me.Фен_ТирTextBox)
        Me.GroupBox4.Controls.Add(Me.КлечаткаTextBox)
        Me.GroupBox4.Controls.Add(Фен_ТирLabel)
        Me.GroupBox4.Controls.Add(ЗолаLabel)
        Me.GroupBox4.Controls.Add(Me.Мет_ЦистTextBox)
        Me.GroupBox4.Controls.Add(Me.ЗолаTextBox)
        Me.GroupBox4.Controls.Add(Мет_ЦистLabel)
        Me.GroupBox4.Controls.Add(ИзолейцLabel)
        Me.GroupBox4.Controls.Add(Me.ВалинTextBox)
        Me.GroupBox4.Controls.Add(Me.ИзолейцTextBox)
        Me.GroupBox4.Controls.Add(ВалинLabel)
        Me.GroupBox4.Controls.Add(ЛейцLabel)
        Me.GroupBox4.Controls.Add(Me.ЛейцTextBox)
        Me.GroupBox4.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.GroupBox4.ForeColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.GroupBox4.Location = New System.Drawing.Point(1363, 23)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(385, 467)
        Me.GroupBox4.TabIndex = 49
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Общие показатели ингредиента"
        '
        'КодLabel1
        '
        Me.КодLabel1.ForeColor = System.Drawing.Color.White
        Me.КодLabel1.Location = New System.Drawing.Point(54, 23)
        Me.КодLabel1.Name = "КодLabel1"
        Me.КодLabel1.Size = New System.Drawing.Size(100, 23)
        Me.КодLabel1.TabIndex = 48
        Me.КодLabel1.Text = "Label1"
        '
        'ТреонTextBox
        '
        Me.ТреонTextBox.Location = New System.Drawing.Point(172, 420)
        Me.ТреонTextBox.Name = "ТреонTextBox"
        Me.ТреонTextBox.Size = New System.Drawing.Size(100, 24)
        Me.ТреонTextBox.TabIndex = 46
        '
        'Содержание_сухих_веществTextBox
        '
        Me.Содержание_сухих_веществTextBox.Location = New System.Drawing.Point(172, 59)
        Me.Содержание_сухих_веществTextBox.Name = "Содержание_сухих_веществTextBox"
        Me.Содержание_сухих_веществTextBox.Size = New System.Drawing.Size(100, 24)
        Me.Содержание_сухих_веществTextBox.TabIndex = 47
        '
        'ЛизинTextBox
        '
        Me.ЛизинTextBox.Location = New System.Drawing.Point(172, 394)
        Me.ЛизинTextBox.Name = "ЛизинTextBox"
        Me.ЛизинTextBox.Size = New System.Drawing.Size(100, 24)
        Me.ЛизинTextBox.TabIndex = 44
        '
        'ТриптофанTextBox
        '
        Me.ТриптофанTextBox.Location = New System.Drawing.Point(172, 368)
        Me.ТриптофанTextBox.Name = "ТриптофанTextBox"
        Me.ТриптофанTextBox.Size = New System.Drawing.Size(100, 24)
        Me.ТриптофанTextBox.TabIndex = 42
        '
        'ВодаTextBox
        '
        Me.ВодаTextBox.Location = New System.Drawing.Point(172, 86)
        Me.ВодаTextBox.Name = "ВодаTextBox"
        Me.ВодаTextBox.Size = New System.Drawing.Size(100, 24)
        Me.ВодаTextBox.TabIndex = 20
        '
        'Фен_ТирTextBox
        '
        Me.Фен_ТирTextBox.Location = New System.Drawing.Point(172, 342)
        Me.Фен_ТирTextBox.Name = "Фен_ТирTextBox"
        Me.Фен_ТирTextBox.Size = New System.Drawing.Size(100, 24)
        Me.Фен_ТирTextBox.TabIndex = 40
        '
        'КлечаткаTextBox
        '
        Me.КлечаткаTextBox.Location = New System.Drawing.Point(172, 147)
        Me.КлечаткаTextBox.Name = "КлечаткаTextBox"
        Me.КлечаткаTextBox.Size = New System.Drawing.Size(100, 24)
        Me.КлечаткаTextBox.TabIndex = 28
        '
        'Мет_ЦистTextBox
        '
        Me.Мет_ЦистTextBox.Location = New System.Drawing.Point(172, 316)
        Me.Мет_ЦистTextBox.Name = "Мет_ЦистTextBox"
        Me.Мет_ЦистTextBox.Size = New System.Drawing.Size(100, 24)
        Me.Мет_ЦистTextBox.TabIndex = 38
        '
        'ЗолаTextBox
        '
        Me.ЗолаTextBox.Location = New System.Drawing.Point(172, 173)
        Me.ЗолаTextBox.Name = "ЗолаTextBox"
        Me.ЗолаTextBox.Size = New System.Drawing.Size(100, 24)
        Me.ЗолаTextBox.TabIndex = 30
        '
        'ВалинTextBox
        '
        Me.ВалинTextBox.Location = New System.Drawing.Point(172, 290)
        Me.ВалинTextBox.Name = "ВалинTextBox"
        Me.ВалинTextBox.Size = New System.Drawing.Size(100, 24)
        Me.ВалинTextBox.TabIndex = 36
        '
        'ИзолейцTextBox
        '
        Me.ИзолейцTextBox.Location = New System.Drawing.Point(172, 237)
        Me.ИзолейцTextBox.Name = "ИзолейцTextBox"
        Me.ИзолейцTextBox.Size = New System.Drawing.Size(100, 24)
        Me.ИзолейцTextBox.TabIndex = 32
        '
        'ЛейцTextBox
        '
        Me.ЛейцTextBox.Location = New System.Drawing.Point(172, 264)
        Me.ЛейцTextBox.Name = "ЛейцTextBox"
        Me.ЛейцTextBox.Size = New System.Drawing.Size(100, 24)
        Me.ЛейцTextBox.TabIndex = 34
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Lucida Sans Unicode", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.White
        Me.Label3.Location = New System.Drawing.Point(964, 9)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(121, 18)
        Me.Label3.TabIndex = 11
        Me.Label3.Text = "Код рецепта:"
        '
        'CodeRecipe
        '
        Me.CodeRecipe.AutoSize = True
        Me.CodeRecipe.Font = New System.Drawing.Font("Lucida Sans Unicode", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.CodeRecipe.ForeColor = System.Drawing.Color.White
        Me.CodeRecipe.Location = New System.Drawing.Point(1091, 9)
        Me.CodeRecipe.Name = "CodeRecipe"
        Me.CodeRecipe.Size = New System.Drawing.Size(40, 18)
        Me.CodeRecipe.TabIndex = 12
        Me.CodeRecipe.Text = "Код"
        '
        'ProductsDBDataSet
        '
        Me.ProductsDBDataSet.DataSetName = "ProductsDBDataSet"
        Me.ProductsDBDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'IngredientsBindingSource
        '
        Me.IngredientsBindingSource.DataMember = "Ingredients"
        Me.IngredientsBindingSource.DataSource = Me.ProductsDBDataSet
        '
        'IngredientsTableAdapter
        '
        Me.IngredientsTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.IngredientsTableAdapter = Me.IngredientsTableAdapter
        Me.TableAdapterManager.IngTableAdapter = Nothing
        Me.TableAdapterManager.RecipesTableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = ProjectUMeat.ProductsDBDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'НазваниеListBox
        '
        Me.НазваниеListBox.DataSource = Me.IngredientsBindingSource
        Me.НазваниеListBox.DisplayMember = "Название"
        Me.НазваниеListBox.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.НазваниеListBox.FormattingEnabled = True
        Me.НазваниеListBox.ImeMode = System.Windows.Forms.ImeMode.Disable
        Me.НазваниеListBox.ItemHeight = 19
        Me.НазваниеListBox.Location = New System.Drawing.Point(12, 149)
        Me.НазваниеListBox.Name = "НазваниеListBox"
        Me.НазваниеListBox.Size = New System.Drawing.Size(315, 346)
        Me.НазваниеListBox.TabIndex = 51
        Me.НазваниеListBox.ValueMember = "Код"
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(18, 26)
        Me.TextBox1.Multiline = True
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(259, 24)
        Me.TextBox1.TabIndex = 52
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(283, 26)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(24, 24)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.PictureBox1.TabIndex = 53
        Me.PictureBox1.TabStop = False
        '
        'GroupBox2
        '
        Me.GroupBox2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox2.Controls.Add(Me.PictureBox1)
        Me.GroupBox2.Controls.Add(Me.TextBox1)
        Me.GroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.GroupBox2.ForeColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.GroupBox2.Location = New System.Drawing.Point(-149, 32)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(315, 70)
        Me.GroupBox2.TabIndex = 55
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Быстрый поиск ингредиента"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Lucida Sans Unicode", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.Location = New System.Drawing.Point(12, 117)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(240, 18)
        Me.Label1.TabIndex = 57
        Me.Label1.Text = "Рецептурные ингредиенты:"
        '
        'dgvОсновныеКомпоненты
        '
        Me.dgvОсновныеКомпоненты.AllowUserToAddRows = False
        Me.dgvОсновныеКомпоненты.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvОсновныеКомпоненты.BackgroundColor = System.Drawing.Color.Gray
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.ControlDarkDark
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        DataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvОсновныеКомпоненты.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle6
        Me.dgvОсновныеКомпоненты.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvОсновныеКомпоненты.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.БелкиPer, Me.ЖирыPer, Me.УглеводыPer})
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle7.BackColor = System.Drawing.Color.White
        DataGridViewCellStyle7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        DataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvОсновныеКомпоненты.DefaultCellStyle = DataGridViewCellStyle7
        Me.dgvОсновныеКомпоненты.Location = New System.Drawing.Point(381, 401)
        Me.dgvОсновныеКомпоненты.Name = "dgvОсновныеКомпоненты"
        Me.dgvОсновныеКомпоненты.RowHeadersVisible = False
        Me.dgvОсновныеКомпоненты.Size = New System.Drawing.Size(350, 60)
        Me.dgvОсновныеКомпоненты.TabIndex = 58
        '
        'БелкиPer
        '
        Me.БелкиPer.HeaderText = "Белки в %"
        Me.БелкиPer.Name = "БелкиPer"
        Me.БелкиPer.ReadOnly = True
        '
        'ЖирыPer
        '
        Me.ЖирыPer.HeaderText = "Жиры в %"
        Me.ЖирыPer.Name = "ЖирыPer"
        Me.ЖирыPer.ReadOnly = True
        '
        'УглеводыPer
        '
        Me.УглеводыPer.HeaderText = "Углеводы в %"
        Me.УглеводыPer.Name = "УглеводыPer"
        Me.УглеводыPer.ReadOnly = True
        '
        'dgvЭнергетическаяЦенность
        '
        Me.dgvЭнергетическаяЦенность.AllowUserToAddRows = False
        Me.dgvЭнергетическаяЦенность.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvЭнергетическаяЦенность.BackgroundColor = System.Drawing.Color.Gray
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.ControlDarkDark
        DataGridViewCellStyle8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        DataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvЭнергетическаяЦенность.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle8
        Me.dgvЭнергетическаяЦенность.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvЭнергетическаяЦенность.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.кКал, Me.кДж})
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle9.BackColor = System.Drawing.Color.White
        DataGridViewCellStyle9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        DataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvЭнергетическаяЦенность.DefaultCellStyle = DataGridViewCellStyle9
        Me.dgvЭнергетическаяЦенность.Location = New System.Drawing.Point(759, 401)
        Me.dgvЭнергетическаяЦенность.Name = "dgvЭнергетическаяЦенность"
        Me.dgvЭнергетическаяЦенность.RowHeadersVisible = False
        Me.dgvЭнергетическаяЦенность.Size = New System.Drawing.Size(324, 60)
        Me.dgvЭнергетическаяЦенность.TabIndex = 59
        '
        'кКал
        '
        Me.кКал.HeaderText = "кКал"
        Me.кКал.Name = "кКал"
        Me.кКал.ReadOnly = True
        '
        'кДж
        '
        Me.кДж.HeaderText = "кДж"
        Me.кДж.Name = "кДж"
        Me.кДж.ReadOnly = True
        '
        'dgvНак_Скор
        '
        Me.dgvНак_Скор.AllowUserToAddRows = False
        Me.dgvНак_Скор.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvНак_Скор.BackgroundColor = System.Drawing.Color.Gray
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.ControlDarkDark
        DataGridViewCellStyle10.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        DataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvНак_Скор.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle10
        Me.dgvНак_Скор.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvНак_Скор.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Column1, Me.Column9, Me.Column2, Me.Column3, Me.Column4, Me.Column5, Me.Column6, Me.Column7, Me.Column8})
        DataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle11.BackColor = System.Drawing.Color.White
        DataGridViewCellStyle11.Font = New System.Drawing.Font("Monotype Corsiva", 14.25!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        DataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvНак_Скор.DefaultCellStyle = DataGridViewCellStyle11
        Me.dgvНак_Скор.Location = New System.Drawing.Point(381, 493)
        Me.dgvНак_Скор.MultiSelect = False
        Me.dgvНак_Скор.Name = "dgvНак_Скор"
        Me.dgvНак_Скор.RowHeadersVisible = False
        Me.dgvНак_Скор.Size = New System.Drawing.Size(750, 110)
        Me.dgvНак_Скор.TabIndex = 60
        '
        'Column1
        '
        Me.Column1.HeaderText = "Заголовок"
        Me.Column1.Name = "Column1"
        Me.Column1.ReadOnly = True
        '
        'Column9
        '
        Me.Column9.HeaderText = "изолейцин"
        Me.Column9.Name = "Column9"
        Me.Column9.ReadOnly = True
        '
        'Column2
        '
        Me.Column2.HeaderText = "лейцин"
        Me.Column2.Name = "Column2"
        Me.Column2.ReadOnly = True
        '
        'Column3
        '
        Me.Column3.HeaderText = "валин"
        Me.Column3.Name = "Column3"
        Me.Column3.ReadOnly = True
        '
        'Column4
        '
        Me.Column4.HeaderText = "мет+цист"
        Me.Column4.Name = "Column4"
        Me.Column4.ReadOnly = True
        '
        'Column5
        '
        Me.Column5.HeaderText = "фен+тир"
        Me.Column5.Name = "Column5"
        Me.Column5.ReadOnly = True
        '
        'Column6
        '
        Me.Column6.HeaderText = "триптофан"
        Me.Column6.Name = "Column6"
        Me.Column6.ReadOnly = True
        '
        'Column7
        '
        Me.Column7.HeaderText = "треон"
        Me.Column7.Name = "Column7"
        Me.Column7.ReadOnly = True
        '
        'Column8
        '
        Me.Column8.HeaderText = "лизин"
        Me.Column8.Name = "Column8"
        Me.Column8.ReadOnly = True
        '
        'dgvБелковыйКомпонент
        '
        Me.dgvБелковыйКомпонент.AllowUserToAddRows = False
        Me.dgvБелковыйКомпонент.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvБелковыйКомпонент.BackgroundColor = System.Drawing.Color.Gray
        DataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.ControlDarkDark
        DataGridViewCellStyle12.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        DataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvБелковыйКомпонент.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle12
        Me.dgvБелковыйКомпонент.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvБелковыйКомпонент.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn1, Me.Column10, Me.DataGridViewTextBoxColumn2, Me.DataGridViewTextBoxColumn3})
        DataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle13.BackColor = System.Drawing.Color.White
        DataGridViewCellStyle13.Font = New System.Drawing.Font("Monotype Corsiva", 14.25!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        DataGridViewCellStyle13.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle13.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle13.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvБелковыйКомпонент.DefaultCellStyle = DataGridViewCellStyle13
        Me.dgvБелковыйКомпонент.Location = New System.Drawing.Point(381, 627)
        Me.dgvБелковыйКомпонент.Name = "dgvБелковыйКомпонент"
        Me.dgvБелковыйКомпонент.RowHeadersVisible = False
        Me.dgvБелковыйКомпонент.Size = New System.Drawing.Size(426, 60)
        Me.dgvБелковыйКомпонент.TabIndex = 61
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.HeaderText = "КРАС"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        '
        'Column10
        '
        Me.Column10.HeaderText = "БЦ"
        Me.Column10.Name = "Column10"
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.HeaderText = "U"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.HeaderText = "G"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Lucida Sans Unicode", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.White
        Me.Label2.Location = New System.Drawing.Point(794, 378)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(224, 18)
        Me.Label2.TabIndex = 62
        Me.Label2.Text = "Энергетическая ценность"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Lucida Sans Unicode", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.White
        Me.Label4.Location = New System.Drawing.Point(445, 378)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(192, 18)
        Me.Label4.TabIndex = 63
        Me.Label4.Text = "Основные показатели"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Lucida Sans Unicode", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.White
        Me.Label5.Location = New System.Drawing.Point(378, 472)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(131, 18)
        Me.Label5.TabIndex = 64
        Me.Label5.Text = "Аминокислоты"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Lucida Sans Unicode", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.White
        Me.Label6.Location = New System.Drawing.Point(378, 606)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(217, 18)
        Me.Label6.TabIndex = 65
        Me.Label6.Text = "Биологическая ценность"
        '
        'txtNameRecipe
        '
        Me.txtNameRecipe.Location = New System.Drawing.Point(347, 39)
        Me.txtNameRecipe.Multiline = True
        Me.txtNameRecipe.Name = "txtNameRecipe"
        Me.txtNameRecipe.Size = New System.Drawing.Size(784, 24)
        Me.txtNameRecipe.TabIndex = 54
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Lucida Sans Unicode", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.Label7.ForeColor = System.Drawing.Color.White
        Me.Label7.Location = New System.Drawing.Point(624, 9)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(183, 18)
        Me.Label7.TabIndex = 66
        Me.Label7.Text = "Название рецептуры"
        '
        'btnOptim
        '
        Me.btnOptim.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnOptim.BackColor = System.Drawing.Color.DodgerBlue
        Me.btnOptim.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.btnOptim.FlatAppearance.BorderColor = System.Drawing.Color.DeepSkyBlue
        Me.btnOptim.FlatAppearance.BorderSize = 0
        Me.btnOptim.FlatAppearance.MouseDownBackColor = System.Drawing.Color.SteelBlue
        Me.btnOptim.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DeepSkyBlue
        Me.btnOptim.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnOptim.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.btnOptim.ForeColor = System.Drawing.Color.White
        Me.btnOptim.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnOptim.Location = New System.Drawing.Point(1001, 44)
        Me.btnOptim.Name = "btnOptim"
        Me.btnOptim.Size = New System.Drawing.Size(190, 56)
        Me.btnOptim.TabIndex = 67
        Me.btnOptim.Text = "Сохранить рецептуру"
        Me.btnOptim.UseVisualStyleBackColor = False
        '
        'Button1
        '
        Me.Button1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button1.BackColor = System.Drawing.Color.DodgerBlue
        Me.Button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.Button1.FlatAppearance.BorderColor = System.Drawing.Color.DeepSkyBlue
        Me.Button1.FlatAppearance.BorderSize = 0
        Me.Button1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.SteelBlue
        Me.Button1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DeepSkyBlue
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.White
        Me.Button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button1.Location = New System.Drawing.Point(1001, 149)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(190, 56)
        Me.Button1.TabIndex = 68
        Me.Button1.Text = "Расчитать характеристики"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.Label9.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Label9.Location = New System.Drawing.Point(12, 9)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(257, 18)
        Me.Label9.TabIndex = 70
        Me.Label9.Text = "Авторы ПО: Кимсанбаев Карим"
        '
        'frmEditorRecipe
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.DimGray
        Me.ClientSize = New System.Drawing.Size(1370, 727)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.btnOptim)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.txtNameRecipe)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.dgvБелковыйКомпонент)
        Me.Controls.Add(Me.dgvНак_Скор)
        Me.Controls.Add(Me.dgvЭнергетическаяЦенность)
        Me.Controls.Add(Me.dgvОсновныеКомпоненты)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.НазваниеListBox)
        Me.Controls.Add(Me.GroupBox4)
        Me.Controls.Add(Me.CodeRecipe)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.dgvRecipe1)
        Me.Name = "frmEditorRecipe"
        Me.Text = "Редактор рецептов"
        CType(Me.dgvRecipe1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        CType(Me.ProductsDBDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.IngredientsBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.dgvОсновныеКомпоненты, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvЭнергетическаяЦенность, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvНак_Скор, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvБелковыйКомпонент, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents dgvRecipe1 As DataGridView
    Friend WithEvents GroupBox4 As GroupBox
    Friend WithEvents КодLabel1 As Label
    Friend WithEvents ТреонTextBox As TextBox
    Friend WithEvents Содержание_сухих_веществTextBox As TextBox
    Friend WithEvents ЛизинTextBox As TextBox
    Friend WithEvents ТриптофанTextBox As TextBox
    Friend WithEvents ВодаTextBox As TextBox
    Friend WithEvents Фен_ТирTextBox As TextBox
    Friend WithEvents КлечаткаTextBox As TextBox
    Friend WithEvents Мет_ЦистTextBox As TextBox
    Friend WithEvents ЗолаTextBox As TextBox
    Friend WithEvents ВалинTextBox As TextBox
    Friend WithEvents ИзолейцTextBox As TextBox
    Friend WithEvents ЛейцTextBox As TextBox
    Friend WithEvents Label3 As Label
    Friend WithEvents CodeRecipe As Label
    Friend WithEvents ProductsDBDataSet As ProductsDBDataSet
    Friend WithEvents IngredientsBindingSource As BindingSource
    Friend WithEvents IngredientsTableAdapter As ProductsDBDataSetTableAdapters.IngredientsTableAdapter
    Friend WithEvents TableAdapterManager As ProductsDBDataSetTableAdapters.TableAdapterManager
    Friend WithEvents НазваниеListBox As ListBox
    Friend WithEvents TextBox1 As TextBox
    Friend WithEvents PictureBox1 As PictureBox
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents Label1 As Label
    Friend WithEvents Название_ингредиента As DataGridViewTextBoxColumn
    Friend WithEvents Массовая_доля As DataGridViewTextBoxColumn
    Friend WithEvents Белки As DataGridViewTextBoxColumn
    Friend WithEvents Жиры As DataGridViewTextBoxColumn
    Friend WithEvents Углеводы As DataGridViewTextBoxColumn
    Friend WithEvents Цена As DataGridViewTextBoxColumn
    Friend WithEvents ЭЦ As DataGridViewTextBoxColumn
    Friend WithEvents dgvОсновныеКомпоненты As DataGridView
    Friend WithEvents БелкиPer As DataGridViewTextBoxColumn
    Friend WithEvents ЖирыPer As DataGridViewTextBoxColumn
    Friend WithEvents УглеводыPer As DataGridViewTextBoxColumn
    Friend WithEvents dgvЭнергетическаяЦенность As DataGridView
    Friend WithEvents кКал As DataGridViewTextBoxColumn
    Friend WithEvents кДж As DataGridViewTextBoxColumn
    Friend WithEvents dgvБелковыйКомпонент As DataGridView
    Friend WithEvents Column1 As DataGridViewTextBoxColumn
    Friend WithEvents Column9 As DataGridViewTextBoxColumn
    Friend WithEvents Column2 As DataGridViewTextBoxColumn
    Friend WithEvents Column3 As DataGridViewTextBoxColumn
    Friend WithEvents Column4 As DataGridViewTextBoxColumn
    Friend WithEvents Column5 As DataGridViewTextBoxColumn
    Friend WithEvents Column6 As DataGridViewTextBoxColumn
    Friend WithEvents Column7 As DataGridViewTextBoxColumn
    Friend WithEvents Column8 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents Column10 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As DataGridViewTextBoxColumn
    Friend WithEvents Label2 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents dgvНак_Скор As DataGridView
    Friend WithEvents txtNameRecipe As TextBox
    Friend WithEvents Label7 As Label
    Friend WithEvents btnOptim As Button
    Friend WithEvents Button1 As Button
    Friend WithEvents Label9 As Label
End Class
